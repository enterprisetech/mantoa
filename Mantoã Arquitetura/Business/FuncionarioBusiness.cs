﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantoã_Arquitetura.Business
{
    class FuncionarioBusiness
    {

        public void ValidarFuncionario(Model.Entities.tb_employees modelo)
        {
            Database.FuncionarioDatabase validado = new Database.FuncionarioDatabase();
            validado.InserirFuncionario(modelo);
        }
        public List<Model.Entities.tb_employees> ConsultarLista()
        {
            Database.FuncionarioDatabase db = new Database.FuncionarioDatabase();

            List<Model.Entities.tb_employees> lista = db.ConsultarFuncionario();
            return lista;
        }

    }
}
