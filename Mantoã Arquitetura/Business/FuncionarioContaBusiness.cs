﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantoã_Arquitetura.Business
{
    class FuncionarioContaBusiness
    {

        public void ValidarContaFuncionario(Model.Entities.tb_contafuncionario model)
        {
            Database.FuncionarioContaDatabase db = new Database.FuncionarioContaDatabase();
            db.InserirConta(model);
        }

        public Model.Entities.tb_contafuncionario ReceberLogin(string user, string pass)
        {
            Database.FuncionarioContaDatabase db = new Database.FuncionarioContaDatabase();
            Model.Entities.tb_contafuncionario conta = db.ConsultarConta(user,pass);
            return conta;
        }
    }
}
