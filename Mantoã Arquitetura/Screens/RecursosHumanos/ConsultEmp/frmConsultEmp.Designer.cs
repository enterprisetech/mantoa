﻿namespace Mantoã_Arquitetura.Screens.ConsultEmp
{
    partial class frmConsultEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultEmp));
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox20 = new System.Windows.Forms.ComboBox();
            this.bunifuMaterialTextbox20 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel46 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.rdbCodigo = new System.Windows.Forms.RadioButton();
            this.rdbAlfabetica = new System.Windows.Forms.RadioButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnFiltro = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgtConsultaFuncionario = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Ações = new System.Windows.Forms.DataGridViewImageColumn();
            this.Funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataContrato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sexo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgtConsultaFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(13, 64);
            this.bunifuCustomLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(180, 19);
            this.bunifuCustomLabel1.TabIndex = 1;
            this.bunifuCustomLabel1.Text = "Situação do funcionário:";
            // 
            // comboBox20
            // 
            this.comboBox20.FormattingEnabled = true;
            this.comboBox20.Location = new System.Drawing.Point(200, 64);
            this.comboBox20.Name = "comboBox20";
            this.comboBox20.Size = new System.Drawing.Size(207, 27);
            this.comboBox20.TabIndex = 42;
            // 
            // bunifuMaterialTextbox20
            // 
            this.bunifuMaterialTextbox20.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMaterialTextbox20.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuMaterialTextbox20.ForeColor = System.Drawing.Color.Black;
            this.bunifuMaterialTextbox20.HintForeColor = System.Drawing.Color.Black;
            this.bunifuMaterialTextbox20.HintText = "";
            this.bunifuMaterialTextbox20.isPassword = false;
            this.bunifuMaterialTextbox20.LineFocusedColor = System.Drawing.Color.Black;
            this.bunifuMaterialTextbox20.LineIdleColor = System.Drawing.Color.Gray;
            this.bunifuMaterialTextbox20.LineMouseHoverColor = System.Drawing.Color.Black;
            this.bunifuMaterialTextbox20.LineThickness = 2;
            this.bunifuMaterialTextbox20.Location = new System.Drawing.Point(84, 88);
            this.bunifuMaterialTextbox20.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuMaterialTextbox20.Name = "bunifuMaterialTextbox20";
            this.bunifuMaterialTextbox20.Size = new System.Drawing.Size(294, 28);
            this.bunifuMaterialTextbox20.TabIndex = 44;
            this.bunifuMaterialTextbox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel46
            // 
            this.bunifuCustomLabel46.AutoSize = true;
            this.bunifuCustomLabel46.Location = new System.Drawing.Point(12, 95);
            this.bunifuCustomLabel46.Name = "bunifuCustomLabel46";
            this.bunifuCustomLabel46.Size = new System.Drawing.Size(54, 19);
            this.bunifuCustomLabel46.TabIndex = 43;
            this.bunifuCustomLabel46.Text = "Nome:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(171, 124);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(207, 27);
            this.comboBox1.TabIndex = 46;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(13, 127);
            this.bunifuCustomLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(116, 19);
            this.bunifuCustomLabel2.TabIndex = 45;
            this.bunifuCustomLabel2.Text = "Departamento:";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(95, 164);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(49, 27);
            this.comboBox2.TabIndex = 48;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(13, 167);
            this.bunifuCustomLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(66, 19);
            this.bunifuCustomLabel3.TabIndex = 47;
            this.bunifuCustomLabel3.Text = "Função:";
            // 
            // rdbCodigo
            // 
            this.rdbCodigo.AutoSize = true;
            this.rdbCodigo.Location = new System.Drawing.Point(494, 86);
            this.rdbCodigo.Name = "rdbCodigo";
            this.rdbCodigo.Size = new System.Drawing.Size(78, 23);
            this.rdbCodigo.TabIndex = 49;
            this.rdbCodigo.TabStop = true;
            this.rdbCodigo.Text = "Código";
            this.rdbCodigo.UseVisualStyleBackColor = true;
            // 
            // rdbAlfabetica
            // 
            this.rdbAlfabetica.AutoSize = true;
            this.rdbAlfabetica.Location = new System.Drawing.Point(593, 88);
            this.rdbAlfabetica.Name = "rdbAlfabetica";
            this.rdbAlfabetica.Size = new System.Drawing.Size(99, 23);
            this.rdbAlfabetica.TabIndex = 50;
            this.rdbAlfabetica.TabStop = true;
            this.rdbAlfabetica.Text = "Alfábetica";
            this.rdbAlfabetica.UseVisualStyleBackColor = true;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(528, 54);
            this.bunifuCustomLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(95, 19);
            this.bunifuCustomLabel4.TabIndex = 51;
            this.bunifuCustomLabel4.Text = "Ordenação:";
            // 
            // btnFiltro
            // 
            this.btnFiltro.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.btnFiltro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.btnFiltro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFiltro.BorderRadius = 0;
            this.btnFiltro.ButtonText = "Filtrar";
            this.btnFiltro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFiltro.DisabledColor = System.Drawing.Color.Gray;
            this.btnFiltro.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltro.Iconcolor = System.Drawing.Color.Transparent;
            this.btnFiltro.Iconimage = null;
            this.btnFiltro.Iconimage_right = null;
            this.btnFiltro.Iconimage_right_Selected = null;
            this.btnFiltro.Iconimage_Selected = null;
            this.btnFiltro.IconMarginLeft = 0;
            this.btnFiltro.IconMarginRight = 0;
            this.btnFiltro.IconRightVisible = true;
            this.btnFiltro.IconRightZoom = 0D;
            this.btnFiltro.IconVisible = true;
            this.btnFiltro.IconZoom = 90D;
            this.btnFiltro.IsTab = false;
            this.btnFiltro.Location = new System.Drawing.Point(517, 124);
            this.btnFiltro.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btnFiltro.Name = "btnFiltro";
            this.btnFiltro.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.btnFiltro.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(187)))), ((int)(((byte)(50)))));
            this.btnFiltro.OnHoverTextColor = System.Drawing.Color.White;
            this.btnFiltro.selected = false;
            this.btnFiltro.Size = new System.Drawing.Size(137, 48);
            this.btnFiltro.TabIndex = 52;
            this.btnFiltro.Text = "Filtrar";
            this.btnFiltro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnFiltro.Textcolor = System.Drawing.Color.White;
            this.btnFiltro.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltro.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFiltro);
            this.panel2.Controls.Add(this.bunifuCustomLabel4);
            this.panel2.Controls.Add(this.rdbAlfabetica);
            this.panel2.Controls.Add(this.rdbCodigo);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.bunifuCustomLabel3);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.bunifuCustomLabel2);
            this.panel2.Controls.Add(this.bunifuMaterialTextbox20);
            this.panel2.Controls.Add(this.bunifuCustomLabel46);
            this.panel2.Controls.Add(this.comboBox20);
            this.panel2.Controls.Add(this.bunifuCustomLabel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1133, 228);
            this.panel2.TabIndex = 53;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // dgtConsultaFuncionario
            // 
            this.dgtConsultaFuncionario.AllowUserToAddRows = false;
            this.dgtConsultaFuncionario.AllowUserToDeleteRows = false;
            this.dgtConsultaFuncionario.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgtConsultaFuncionario.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgtConsultaFuncionario.BackgroundColor = System.Drawing.Color.White;
            this.dgtConsultaFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgtConsultaFuncionario.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgtConsultaFuncionario.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgtConsultaFuncionario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgtConsultaFuncionario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgtConsultaFuncionario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ações,
            this.Funcionario,
            this.DataContrato,
            this.Sexo});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgtConsultaFuncionario.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgtConsultaFuncionario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgtConsultaFuncionario.DoubleBuffered = true;
            this.dgtConsultaFuncionario.EnableHeadersVisualStyles = false;
            this.dgtConsultaFuncionario.GridColor = System.Drawing.Color.White;
            this.dgtConsultaFuncionario.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.dgtConsultaFuncionario.HeaderForeColor = System.Drawing.Color.White;
            this.dgtConsultaFuncionario.Location = new System.Drawing.Point(0, 228);
            this.dgtConsultaFuncionario.MultiSelect = false;
            this.dgtConsultaFuncionario.Name = "dgtConsultaFuncionario";
            this.dgtConsultaFuncionario.ReadOnly = true;
            this.dgtConsultaFuncionario.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgtConsultaFuncionario.RowHeadersVisible = false;
            this.dgtConsultaFuncionario.RowHeadersWidth = 1000;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgtConsultaFuncionario.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgtConsultaFuncionario.ShowCellErrors = false;
            this.dgtConsultaFuncionario.ShowCellToolTips = false;
            this.dgtConsultaFuncionario.ShowEditingIcon = false;
            this.dgtConsultaFuncionario.ShowRowErrors = false;
            this.dgtConsultaFuncionario.Size = new System.Drawing.Size(1133, 327);
            this.dgtConsultaFuncionario.TabIndex = 54;
            // 
            // Ações
            // 
            this.Ações.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Ações.HeaderText = "Editar";
            this.Ações.Image = ((System.Drawing.Image)(resources.GetObject("Ações.Image")));
            this.Ações.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Ações.Name = "Ações";
            this.Ações.ReadOnly = true;
            this.Ações.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Ações.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Ações.Width = 72;
            // 
            // Funcionario
            // 
            this.Funcionario.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Funcionario.FillWeight = 200F;
            this.Funcionario.HeaderText = "Funcionário";
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.ReadOnly = true;
            // 
            // DataContrato
            // 
            this.DataContrato.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DataContrato.FillWeight = 200F;
            this.DataContrato.HeaderText = "Data de contrato";
            this.DataContrato.Name = "DataContrato";
            this.DataContrato.ReadOnly = true;
            // 
            // Sexo
            // 
            this.Sexo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Sexo.FillWeight = 200F;
            this.Sexo.HeaderText = "Sexo";
            this.Sexo.Name = "Sexo";
            this.Sexo.ReadOnly = true;
            // 
            // frmConsultEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1133, 555);
            this.Controls.Add(this.dgtConsultaFuncionario);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1133, 555);
            this.Name = "frmConsultEmp";
            this.Text = "frmConsultEmp";
            this.Shown += new System.EventHandler(this.frmConsultEmp_Shown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgtConsultaFuncionario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.ComboBox comboBox20;
        private Bunifu.Framework.UI.BunifuMaterialTextbox bunifuMaterialTextbox20;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel46;
        private System.Windows.Forms.ComboBox comboBox1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.ComboBox comboBox2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private System.Windows.Forms.RadioButton rdbCodigo;
        private System.Windows.Forms.RadioButton rdbAlfabetica;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuFlatButton btnFiltro;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgtConsultaFuncionario;
        private System.Windows.Forms.DataGridViewImageColumn Ações;
        private System.Windows.Forms.DataGridViewTextBoxColumn Funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataContrato;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sexo;
    }
}