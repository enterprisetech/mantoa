﻿namespace Mantoã_Arquitetura.Screens.RecursosHumanos.RegisterEmployees
{
    partial class frmRegistrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalvar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tabControlEX1 = new Dotnetrix.Controls.TabControlEX();
            this.tabPageEX1 = new Dotnetrix.Controls.TabPageEX();
            this.groupBoxEX2 = new Dotnetrix.Controls.GroupBoxEX();
            this.txtTelefone = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtCidade = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtEstado = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtPais = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel13 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtCivilEstado = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtCelular = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel41 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel39 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtComplemento = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel38 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtNumero = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel37 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel36 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtEndereço = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtCep = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel34 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel14 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel28 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBoxEX1 = new Dotnetrix.Controls.GroupBoxEX();
            this.txtsobrenome = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel21 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.cboSexo = new System.Windows.Forms.ComboBox();
            this.txtDescDeficiencia = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtRaca = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel11 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.rdbDS = new System.Windows.Forms.RadioButton();
            this.bunifuCustomLabel10 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel9 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel8 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dtNasc = new System.Windows.Forms.DateTimePicker();
            this.txtnomefuncionario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tabPageEX2 = new Dotnetrix.Controls.TabPageEX();
            this.groupBoxEX4 = new Dotnetrix.Controls.GroupBoxEX();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtUfPiss = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel7 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtPisNumero = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.cboTypePis = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel26 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dtEmissaoPis = new System.Windows.Forms.DateTimePicker();
            this.bunifuCustomLabel27 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel33 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtEmissaoUF = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.dtEmissaoRg = new System.Windows.Forms.DateTimePicker();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel18 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel16 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtRgNumero = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtUfEmissaoCpf = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel15 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtCpf = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.dtEmissaoCpf = new System.Windows.Forms.DateTimePicker();
            this.bunifuCustomLabel19 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel17 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tabPageEX3 = new Dotnetrix.Controls.TabPageEX();
            this.groupBoxEX5 = new Dotnetrix.Controls.GroupBoxEX();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtBanco = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtAgencia = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel50 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtConta = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel49 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel48 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtSalario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel46 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtsenha = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel20 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtusuario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtCargo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel31 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtDepartamento = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel32 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.bunifuCustomLabel43 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dtContrato = new System.Windows.Forms.DateTimePicker();
            this.bunifuCustomLabel30 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tabControlEX1.SuspendLayout();
            this.tabPageEX1.SuspendLayout();
            this.groupBoxEX2.SuspendLayout();
            this.groupBoxEX1.SuspendLayout();
            this.tabPageEX2.SuspendLayout();
            this.groupBoxEX4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageEX3.SuspendLayout();
            this.groupBoxEX5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.Activecolor = System.Drawing.Color.Transparent;
            this.btnSalvar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.btnSalvar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSalvar.BorderRadius = 0;
            this.btnSalvar.ButtonText = "Gravar dados";
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.DisabledColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSalvar.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSalvar.Iconimage = null;
            this.btnSalvar.Iconimage_right = null;
            this.btnSalvar.Iconimage_right_Selected = null;
            this.btnSalvar.Iconimage_Selected = null;
            this.btnSalvar.IconMarginLeft = 0;
            this.btnSalvar.IconMarginRight = 0;
            this.btnSalvar.IconRightVisible = false;
            this.btnSalvar.IconRightZoom = 0D;
            this.btnSalvar.IconVisible = false;
            this.btnSalvar.IconZoom = 40D;
            this.btnSalvar.IsTab = false;
            this.btnSalvar.Location = new System.Drawing.Point(4, 585);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(5);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.btnSalvar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(187)))), ((int)(((byte)(50)))));
            this.btnSalvar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSalvar.selected = false;
            this.btnSalvar.Size = new System.Drawing.Size(201, 44);
            this.btnSalvar.TabIndex = 48;
            this.btnSalvar.Text = "Gravar dados";
            this.btnSalvar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSalvar.Textcolor = System.Drawing.Color.White;
            this.btnSalvar.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // tabControlEX1
            // 
            this.tabControlEX1.Appearance = Dotnetrix.Controls.TabAppearanceEX.FlatButton;
            this.tabControlEX1.BackColor = System.Drawing.Color.White;
            this.tabControlEX1.Controls.Add(this.tabPageEX1);
            this.tabControlEX1.Controls.Add(this.tabPageEX2);
            this.tabControlEX1.Controls.Add(this.tabPageEX3);
            this.tabControlEX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlEX1.Location = new System.Drawing.Point(0, 0);
            this.tabControlEX1.Name = "tabControlEX1";
            this.tabControlEX1.SelectedIndex = 0;
            this.tabControlEX1.Size = new System.Drawing.Size(1028, 486);
            this.tabControlEX1.TabIndex = 47;
            this.tabControlEX1.UseVisualStyles = false;
            // 
            // tabPageEX1
            // 
            this.tabPageEX1.Controls.Add(this.groupBoxEX2);
            this.tabPageEX1.Controls.Add(this.groupBoxEX1);
            this.tabPageEX1.Location = new System.Drawing.Point(4, 25);
            this.tabPageEX1.Name = "tabPageEX1";
            this.tabPageEX1.Size = new System.Drawing.Size(1020, 457);
            this.tabPageEX1.TabIndex = 0;
            this.tabPageEX1.Text = "Informações pessoais";
            // 
            // groupBoxEX2
            // 
            this.groupBoxEX2.Controls.Add(this.txtTelefone);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel3);
            this.groupBoxEX2.Controls.Add(this.txtCidade);
            this.groupBoxEX2.Controls.Add(this.txtEstado);
            this.groupBoxEX2.Controls.Add(this.txtPais);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel13);
            this.groupBoxEX2.Controls.Add(this.txtCivilEstado);
            this.groupBoxEX2.Controls.Add(this.txtCelular);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel41);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel39);
            this.groupBoxEX2.Controls.Add(this.txtComplemento);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel38);
            this.groupBoxEX2.Controls.Add(this.txtNumero);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel37);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel36);
            this.groupBoxEX2.Controls.Add(this.txtEndereço);
            this.groupBoxEX2.Controls.Add(this.txtCep);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel34);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel14);
            this.groupBoxEX2.Controls.Add(this.bunifuCustomLabel28);
            this.groupBoxEX2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEX2.Location = new System.Drawing.Point(0, 174);
            this.groupBoxEX2.Name = "groupBoxEX2";
            this.groupBoxEX2.Size = new System.Drawing.Size(1020, 229);
            this.groupBoxEX2.TabIndex = 22;
            this.groupBoxEX2.TabStop = false;
            this.groupBoxEX2.Text = "Outros";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTelefone.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.HintForeColor = System.Drawing.Color.Black;
            this.txtTelefone.HintText = "";
            this.txtTelefone.isPassword = false;
            this.txtTelefone.LineFocusedColor = System.Drawing.Color.Black;
            this.txtTelefone.LineIdleColor = System.Drawing.Color.Gray;
            this.txtTelefone.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtTelefone.LineThickness = 2;
            this.txtTelefone.Location = new System.Drawing.Point(657, 35);
            this.txtTelefone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(134, 28);
            this.txtTelefone.TabIndex = 18;
            this.txtTelefone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(589, 45);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(48, 13);
            this.bunifuCustomLabel3.TabIndex = 46;
            this.bunifuCustomLabel3.Text = "telefone:";
            // 
            // txtCidade
            // 
            this.txtCidade.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCidade.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.ForeColor = System.Drawing.Color.Black;
            this.txtCidade.HintForeColor = System.Drawing.Color.Black;
            this.txtCidade.HintText = "";
            this.txtCidade.isPassword = false;
            this.txtCidade.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCidade.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCidade.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCidade.LineThickness = 2;
            this.txtCidade.Location = new System.Drawing.Point(73, 154);
            this.txtCidade.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(152, 24);
            this.txtCidade.TabIndex = 12;
            this.txtCidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtEstado
            // 
            this.txtEstado.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEstado.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstado.ForeColor = System.Drawing.Color.Black;
            this.txtEstado.HintForeColor = System.Drawing.Color.Black;
            this.txtEstado.HintText = "";
            this.txtEstado.isPassword = false;
            this.txtEstado.LineFocusedColor = System.Drawing.Color.Black;
            this.txtEstado.LineIdleColor = System.Drawing.Color.Gray;
            this.txtEstado.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtEstado.LineThickness = 2;
            this.txtEstado.Location = new System.Drawing.Point(73, 113);
            this.txtEstado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(152, 24);
            this.txtEstado.TabIndex = 11;
            this.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtPais
            // 
            this.txtPais.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPais.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPais.ForeColor = System.Drawing.Color.Black;
            this.txtPais.HintForeColor = System.Drawing.Color.Black;
            this.txtPais.HintText = "";
            this.txtPais.isPassword = false;
            this.txtPais.LineFocusedColor = System.Drawing.Color.Black;
            this.txtPais.LineIdleColor = System.Drawing.Color.Gray;
            this.txtPais.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtPais.LineThickness = 2;
            this.txtPais.Location = new System.Drawing.Point(54, 75);
            this.txtPais.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(152, 24);
            this.txtPais.TabIndex = 10;
            this.txtPais.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel13
            // 
            this.bunifuCustomLabel13.AutoSize = true;
            this.bunifuCustomLabel13.Location = new System.Drawing.Point(10, 78);
            this.bunifuCustomLabel13.Name = "bunifuCustomLabel13";
            this.bunifuCustomLabel13.Size = new System.Drawing.Size(32, 13);
            this.bunifuCustomLabel13.TabIndex = 42;
            this.bunifuCustomLabel13.Text = "País:";
            // 
            // txtCivilEstado
            // 
            this.txtCivilEstado.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCivilEstado.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCivilEstado.ForeColor = System.Drawing.Color.Black;
            this.txtCivilEstado.HintForeColor = System.Drawing.Color.Black;
            this.txtCivilEstado.HintText = "";
            this.txtCivilEstado.isPassword = false;
            this.txtCivilEstado.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCivilEstado.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCivilEstado.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCivilEstado.LineThickness = 2;
            this.txtCivilEstado.Location = new System.Drawing.Point(105, 34);
            this.txtCivilEstado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCivilEstado.Name = "txtCivilEstado";
            this.txtCivilEstado.Size = new System.Drawing.Size(152, 28);
            this.txtCivilEstado.TabIndex = 8;
            this.txtCivilEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtCelular
            // 
            this.txtCelular.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCelular.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.ForeColor = System.Drawing.Color.Black;
            this.txtCelular.HintForeColor = System.Drawing.Color.Black;
            this.txtCelular.HintText = "";
            this.txtCelular.isPassword = false;
            this.txtCelular.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCelular.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCelular.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCelular.LineThickness = 2;
            this.txtCelular.Location = new System.Drawing.Point(389, 168);
            this.txtCelular.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(134, 28);
            this.txtCelular.TabIndex = 17;
            this.txtCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel41
            // 
            this.bunifuCustomLabel41.AutoSize = true;
            this.bunifuCustomLabel41.Location = new System.Drawing.Point(328, 179);
            this.bunifuCustomLabel41.Name = "bunifuCustomLabel41";
            this.bunifuCustomLabel41.Size = new System.Drawing.Size(42, 13);
            this.bunifuCustomLabel41.TabIndex = 35;
            this.bunifuCustomLabel41.Text = "Celular:";
            // 
            // bunifuCustomLabel39
            // 
            this.bunifuCustomLabel39.AutoSize = true;
            this.bunifuCustomLabel39.Location = new System.Drawing.Point(10, 115);
            this.bunifuCustomLabel39.Name = "bunifuCustomLabel39";
            this.bunifuCustomLabel39.Size = new System.Drawing.Size(43, 13);
            this.bunifuCustomLabel39.TabIndex = 33;
            this.bunifuCustomLabel39.Text = "Estado:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtComplemento.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.HintForeColor = System.Drawing.Color.Black;
            this.txtComplemento.HintText = "";
            this.txtComplemento.isPassword = false;
            this.txtComplemento.LineFocusedColor = System.Drawing.Color.Black;
            this.txtComplemento.LineIdleColor = System.Drawing.Color.Gray;
            this.txtComplemento.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtComplemento.LineThickness = 2;
            this.txtComplemento.Location = new System.Drawing.Point(437, 134);
            this.txtComplemento.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(134, 28);
            this.txtComplemento.TabIndex = 16;
            this.txtComplemento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel38
            // 
            this.bunifuCustomLabel38.AutoSize = true;
            this.bunifuCustomLabel38.Location = new System.Drawing.Point(322, 145);
            this.bunifuCustomLabel38.Name = "bunifuCustomLabel38";
            this.bunifuCustomLabel38.Size = new System.Drawing.Size(74, 13);
            this.bunifuCustomLabel38.TabIndex = 31;
            this.bunifuCustomLabel38.Text = "Complemento:";
            // 
            // txtNumero
            // 
            this.txtNumero.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNumero.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.ForeColor = System.Drawing.Color.Black;
            this.txtNumero.HintForeColor = System.Drawing.Color.Black;
            this.txtNumero.HintText = "";
            this.txtNumero.isPassword = false;
            this.txtNumero.LineFocusedColor = System.Drawing.Color.Black;
            this.txtNumero.LineIdleColor = System.Drawing.Color.Gray;
            this.txtNumero.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtNumero.LineThickness = 2;
            this.txtNumero.Location = new System.Drawing.Point(393, 100);
            this.txtNumero.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(134, 28);
            this.txtNumero.TabIndex = 15;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel37
            // 
            this.bunifuCustomLabel37.AutoSize = true;
            this.bunifuCustomLabel37.Location = new System.Drawing.Point(322, 107);
            this.bunifuCustomLabel37.Name = "bunifuCustomLabel37";
            this.bunifuCustomLabel37.Size = new System.Drawing.Size(47, 13);
            this.bunifuCustomLabel37.TabIndex = 29;
            this.bunifuCustomLabel37.Text = "Número:";
            // 
            // bunifuCustomLabel36
            // 
            this.bunifuCustomLabel36.AutoSize = true;
            this.bunifuCustomLabel36.Location = new System.Drawing.Point(10, 156);
            this.bunifuCustomLabel36.Name = "bunifuCustomLabel36";
            this.bunifuCustomLabel36.Size = new System.Drawing.Size(43, 13);
            this.bunifuCustomLabel36.TabIndex = 27;
            this.bunifuCustomLabel36.Text = "Cidade:";
            // 
            // txtEndereço
            // 
            this.txtEndereço.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEndereço.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereço.ForeColor = System.Drawing.Color.Black;
            this.txtEndereço.HintForeColor = System.Drawing.Color.Black;
            this.txtEndereço.HintText = "";
            this.txtEndereço.isPassword = false;
            this.txtEndereço.LineFocusedColor = System.Drawing.Color.Black;
            this.txtEndereço.LineIdleColor = System.Drawing.Color.Gray;
            this.txtEndereço.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtEndereço.LineThickness = 2;
            this.txtEndereço.Location = new System.Drawing.Point(403, 66);
            this.txtEndereço.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(135, 28);
            this.txtEndereço.TabIndex = 14;
            this.txtEndereço.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtCep
            // 
            this.txtCep.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCep.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCep.ForeColor = System.Drawing.Color.Black;
            this.txtCep.HintForeColor = System.Drawing.Color.Black;
            this.txtCep.HintText = "";
            this.txtCep.isPassword = false;
            this.txtCep.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCep.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCep.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCep.LineThickness = 2;
            this.txtCep.Location = new System.Drawing.Point(367, 35);
            this.txtCep.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(119, 28);
            this.txtCep.TabIndex = 13;
            this.txtCep.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel34
            // 
            this.bunifuCustomLabel34.AutoSize = true;
            this.bunifuCustomLabel34.Location = new System.Drawing.Point(322, 78);
            this.bunifuCustomLabel34.Name = "bunifuCustomLabel34";
            this.bunifuCustomLabel34.Size = new System.Drawing.Size(55, 13);
            this.bunifuCustomLabel34.TabIndex = 23;
            this.bunifuCustomLabel34.Text = "endereço:";
            // 
            // bunifuCustomLabel14
            // 
            this.bunifuCustomLabel14.AutoSize = true;
            this.bunifuCustomLabel14.Location = new System.Drawing.Point(322, 45);
            this.bunifuCustomLabel14.Name = "bunifuCustomLabel14";
            this.bunifuCustomLabel14.Size = new System.Drawing.Size(31, 13);
            this.bunifuCustomLabel14.TabIndex = 22;
            this.bunifuCustomLabel14.Text = "CEP:";
            // 
            // bunifuCustomLabel28
            // 
            this.bunifuCustomLabel28.AutoSize = true;
            this.bunifuCustomLabel28.Location = new System.Drawing.Point(10, 45);
            this.bunifuCustomLabel28.Name = "bunifuCustomLabel28";
            this.bunifuCustomLabel28.Size = new System.Drawing.Size(65, 13);
            this.bunifuCustomLabel28.TabIndex = 10;
            this.bunifuCustomLabel28.Text = "Estado Civil:";
            // 
            // groupBoxEX1
            // 
            this.groupBoxEX1.Controls.Add(this.txtsobrenome);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel21);
            this.groupBoxEX1.Controls.Add(this.cboSexo);
            this.groupBoxEX1.Controls.Add(this.txtDescDeficiencia);
            this.groupBoxEX1.Controls.Add(this.txtRaca);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel11);
            this.groupBoxEX1.Controls.Add(this.rdbDS);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel10);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel9);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel8);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel5);
            this.groupBoxEX1.Controls.Add(this.dtNasc);
            this.groupBoxEX1.Controls.Add(this.txtnomefuncionario);
            this.groupBoxEX1.Controls.Add(this.bunifuCustomLabel2);
            this.groupBoxEX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEX1.Location = new System.Drawing.Point(0, 0);
            this.groupBoxEX1.Name = "groupBoxEX1";
            this.groupBoxEX1.Size = new System.Drawing.Size(1020, 174);
            this.groupBoxEX1.TabIndex = 0;
            this.groupBoxEX1.TabStop = false;
            this.groupBoxEX1.Text = "Dados do funcionário";
            // 
            // txtsobrenome
            // 
            this.txtsobrenome.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtsobrenome.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsobrenome.ForeColor = System.Drawing.Color.Black;
            this.txtsobrenome.HintForeColor = System.Drawing.Color.Black;
            this.txtsobrenome.HintText = "";
            this.txtsobrenome.isPassword = false;
            this.txtsobrenome.LineFocusedColor = System.Drawing.Color.Black;
            this.txtsobrenome.LineIdleColor = System.Drawing.Color.Gray;
            this.txtsobrenome.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtsobrenome.LineThickness = 2;
            this.txtsobrenome.Location = new System.Drawing.Point(98, 52);
            this.txtsobrenome.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsobrenome.Name = "txtsobrenome";
            this.txtsobrenome.Size = new System.Drawing.Size(147, 28);
            this.txtsobrenome.TabIndex = 22;
            this.txtsobrenome.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel21
            // 
            this.bunifuCustomLabel21.AutoSize = true;
            this.bunifuCustomLabel21.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuCustomLabel21.Location = new System.Drawing.Point(6, 63);
            this.bunifuCustomLabel21.Name = "bunifuCustomLabel21";
            this.bunifuCustomLabel21.Size = new System.Drawing.Size(86, 17);
            this.bunifuCustomLabel21.TabIndex = 21;
            this.bunifuCustomLabel21.Text = "Sobrenome:";
            // 
            // cboSexo
            // 
            this.cboSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSexo.FormattingEnabled = true;
            this.cboSexo.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cboSexo.Location = new System.Drawing.Point(618, 33);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Size = new System.Drawing.Size(121, 21);
            this.cboSexo.TabIndex = 5;
            // 
            // txtDescDeficiencia
            // 
            this.txtDescDeficiencia.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDescDeficiencia.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescDeficiencia.ForeColor = System.Drawing.Color.Black;
            this.txtDescDeficiencia.HintForeColor = System.Drawing.Color.Black;
            this.txtDescDeficiencia.HintText = "";
            this.txtDescDeficiencia.isPassword = false;
            this.txtDescDeficiencia.LineFocusedColor = System.Drawing.Color.Black;
            this.txtDescDeficiencia.LineIdleColor = System.Drawing.Color.Gray;
            this.txtDescDeficiencia.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtDescDeficiencia.LineThickness = 2;
            this.txtDescDeficiencia.Location = new System.Drawing.Point(688, 88);
            this.txtDescDeficiencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDescDeficiencia.Name = "txtDescDeficiencia";
            this.txtDescDeficiencia.Size = new System.Drawing.Size(161, 28);
            this.txtDescDeficiencia.TabIndex = 7;
            this.txtDescDeficiencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtRaca
            // 
            this.txtRaca.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRaca.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRaca.ForeColor = System.Drawing.Color.Black;
            this.txtRaca.HintForeColor = System.Drawing.Color.Black;
            this.txtRaca.HintText = "";
            this.txtRaca.isPassword = false;
            this.txtRaca.LineFocusedColor = System.Drawing.Color.Black;
            this.txtRaca.LineIdleColor = System.Drawing.Color.Gray;
            this.txtRaca.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtRaca.LineThickness = 2;
            this.txtRaca.Location = new System.Drawing.Point(84, 119);
            this.txtRaca.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRaca.Name = "txtRaca";
            this.txtRaca.Size = new System.Drawing.Size(161, 28);
            this.txtRaca.TabIndex = 4;
            this.txtRaca.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel11
            // 
            this.bunifuCustomLabel11.AutoSize = true;
            this.bunifuCustomLabel11.Location = new System.Drawing.Point(567, 99);
            this.bunifuCustomLabel11.Name = "bunifuCustomLabel11";
            this.bunifuCustomLabel11.Size = new System.Drawing.Size(87, 13);
            this.bunifuCustomLabel11.TabIndex = 20;
            this.bunifuCustomLabel11.Text = "Tipo Deficiência:";
            // 
            // rdbDS
            // 
            this.rdbDS.AutoSize = true;
            this.rdbDS.Location = new System.Drawing.Point(659, 61);
            this.rdbDS.Name = "rdbDS";
            this.rdbDS.Size = new System.Drawing.Size(42, 17);
            this.rdbDS.TabIndex = 6;
            this.rdbDS.TabStop = true;
            this.rdbDS.Text = "Sim";
            this.rdbDS.UseVisualStyleBackColor = true;
            // 
            // bunifuCustomLabel10
            // 
            this.bunifuCustomLabel10.AutoSize = true;
            this.bunifuCustomLabel10.Location = new System.Drawing.Point(570, 63);
            this.bunifuCustomLabel10.Name = "bunifuCustomLabel10";
            this.bunifuCustomLabel10.Size = new System.Drawing.Size(63, 13);
            this.bunifuCustomLabel10.TabIndex = 17;
            this.bunifuCustomLabel10.Text = "Deficiencia:";
            // 
            // bunifuCustomLabel9
            // 
            this.bunifuCustomLabel9.AutoSize = true;
            this.bunifuCustomLabel9.Location = new System.Drawing.Point(570, 33);
            this.bunifuCustomLabel9.Name = "bunifuCustomLabel9";
            this.bunifuCustomLabel9.Size = new System.Drawing.Size(34, 13);
            this.bunifuCustomLabel9.TabIndex = 14;
            this.bunifuCustomLabel9.Text = "Sexo:";
            // 
            // bunifuCustomLabel8
            // 
            this.bunifuCustomLabel8.AutoSize = true;
            this.bunifuCustomLabel8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuCustomLabel8.Location = new System.Drawing.Point(6, 130);
            this.bunifuCustomLabel8.Name = "bunifuCustomLabel8";
            this.bunifuCustomLabel8.Size = new System.Drawing.Size(73, 17);
            this.bunifuCustomLabel8.TabIndex = 12;
            this.bunifuCustomLabel8.Text = "Raça/cor:";
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(3, 99);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(81, 17);
            this.bunifuCustomLabel5.TabIndex = 7;
            this.bunifuCustomLabel5.Text = "Data Nasc.";
            // 
            // dtNasc
            // 
            this.dtNasc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtNasc.Location = new System.Drawing.Point(97, 98);
            this.dtNasc.Name = "dtNasc";
            this.dtNasc.Size = new System.Drawing.Size(102, 20);
            this.dtNasc.TabIndex = 3;
            // 
            // txtnomefuncionario
            // 
            this.txtnomefuncionario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtnomefuncionario.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomefuncionario.ForeColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.HintForeColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.HintText = "";
            this.txtnomefuncionario.isPassword = false;
            this.txtnomefuncionario.LineFocusedColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtnomefuncionario.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.LineThickness = 2;
            this.txtnomefuncionario.Location = new System.Drawing.Point(65, 22);
            this.txtnomefuncionario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnomefuncionario.Name = "txtnomefuncionario";
            this.txtnomefuncionario.Size = new System.Drawing.Size(147, 28);
            this.txtnomefuncionario.TabIndex = 1;
            this.txtnomefuncionario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(6, 33);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(52, 17);
            this.bunifuCustomLabel2.TabIndex = 0;
            this.bunifuCustomLabel2.Text = "Nome:";
            // 
            // tabPageEX2
            // 
            this.tabPageEX2.Controls.Add(this.groupBoxEX4);
            this.tabPageEX2.Location = new System.Drawing.Point(4, 25);
            this.tabPageEX2.Name = "tabPageEX2";
            this.tabPageEX2.Size = new System.Drawing.Size(1020, 457);
            this.tabPageEX2.TabIndex = 1;
            this.tabPageEX2.Text = "Documentos";
            // 
            // groupBoxEX4
            // 
            this.groupBoxEX4.Controls.Add(this.groupBox4);
            this.groupBoxEX4.Controls.Add(this.groupBox2);
            this.groupBoxEX4.Controls.Add(this.groupBox1);
            this.groupBoxEX4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEX4.Location = new System.Drawing.Point(0, 0);
            this.groupBoxEX4.Name = "groupBoxEX4";
            this.groupBoxEX4.Size = new System.Drawing.Size(1020, 490);
            this.groupBoxEX4.TabIndex = 1;
            this.groupBoxEX4.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtUfPiss);
            this.groupBox4.Controls.Add(this.bunifuCustomLabel7);
            this.groupBox4.Controls.Add(this.txtPisNumero);
            this.groupBox4.Controls.Add(this.cboTypePis);
            this.groupBox4.Controls.Add(this.bunifuCustomLabel26);
            this.groupBox4.Controls.Add(this.dtEmissaoPis);
            this.groupBox4.Controls.Add(this.bunifuCustomLabel27);
            this.groupBox4.Controls.Add(this.bunifuCustomLabel33);
            this.groupBox4.Location = new System.Drawing.Point(254, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(265, 178);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PIS";
            // 
            // txtUfPiss
            // 
            this.txtUfPiss.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUfPiss.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUfPiss.ForeColor = System.Drawing.Color.Black;
            this.txtUfPiss.HintForeColor = System.Drawing.Color.Black;
            this.txtUfPiss.HintText = "";
            this.txtUfPiss.isPassword = false;
            this.txtUfPiss.LineFocusedColor = System.Drawing.Color.Black;
            this.txtUfPiss.LineIdleColor = System.Drawing.Color.Gray;
            this.txtUfPiss.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtUfPiss.LineThickness = 2;
            this.txtUfPiss.Location = new System.Drawing.Point(96, 129);
            this.txtUfPiss.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUfPiss.Name = "txtUfPiss";
            this.txtUfPiss.Size = new System.Drawing.Size(123, 28);
            this.txtUfPiss.TabIndex = 10;
            this.txtUfPiss.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel7
            // 
            this.bunifuCustomLabel7.AutoSize = true;
            this.bunifuCustomLabel7.Location = new System.Drawing.Point(7, 68);
            this.bunifuCustomLabel7.Name = "bunifuCustomLabel7";
            this.bunifuCustomLabel7.Size = new System.Drawing.Size(49, 13);
            this.bunifuCustomLabel7.TabIndex = 36;
            this.bunifuCustomLabel7.Text = "Númeiro:";
            // 
            // txtPisNumero
            // 
            this.txtPisNumero.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPisNumero.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPisNumero.ForeColor = System.Drawing.Color.Black;
            this.txtPisNumero.HintForeColor = System.Drawing.Color.Black;
            this.txtPisNumero.HintText = "";
            this.txtPisNumero.isPassword = false;
            this.txtPisNumero.LineFocusedColor = System.Drawing.Color.Black;
            this.txtPisNumero.LineIdleColor = System.Drawing.Color.Gray;
            this.txtPisNumero.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtPisNumero.LineThickness = 2;
            this.txtPisNumero.Location = new System.Drawing.Point(81, 57);
            this.txtPisNumero.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPisNumero.Name = "txtPisNumero";
            this.txtPisNumero.Size = new System.Drawing.Size(138, 28);
            this.txtPisNumero.TabIndex = 8;
            this.txtPisNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // cboTypePis
            // 
            this.cboTypePis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTypePis.FormattingEnabled = true;
            this.cboTypePis.Items.AddRange(new object[] {
            "PIS",
            "Pasep"});
            this.cboTypePis.Location = new System.Drawing.Point(66, 28);
            this.cboTypePis.Name = "cboTypePis";
            this.cboTypePis.Size = new System.Drawing.Size(75, 21);
            this.cboTypePis.TabIndex = 7;
            // 
            // bunifuCustomLabel26
            // 
            this.bunifuCustomLabel26.AutoSize = true;
            this.bunifuCustomLabel26.Location = new System.Drawing.Point(11, 140);
            this.bunifuCustomLabel26.Name = "bunifuCustomLabel26";
            this.bunifuCustomLabel26.Size = new System.Drawing.Size(66, 13);
            this.bunifuCustomLabel26.TabIndex = 29;
            this.bunifuCustomLabel26.Text = "UF Emissão:";
            // 
            // dtEmissaoPis
            // 
            this.dtEmissaoPis.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissaoPis.Location = new System.Drawing.Point(76, 101);
            this.dtEmissaoPis.Name = "dtEmissaoPis";
            this.dtEmissaoPis.Size = new System.Drawing.Size(111, 20);
            this.dtEmissaoPis.TabIndex = 9;
            // 
            // bunifuCustomLabel27
            // 
            this.bunifuCustomLabel27.AutoSize = true;
            this.bunifuCustomLabel27.Location = new System.Drawing.Point(7, 106);
            this.bunifuCustomLabel27.Name = "bunifuCustomLabel27";
            this.bunifuCustomLabel27.Size = new System.Drawing.Size(49, 13);
            this.bunifuCustomLabel27.TabIndex = 27;
            this.bunifuCustomLabel27.Text = "Emissão:";
            // 
            // bunifuCustomLabel33
            // 
            this.bunifuCustomLabel33.AutoSize = true;
            this.bunifuCustomLabel33.Location = new System.Drawing.Point(11, 31);
            this.bunifuCustomLabel33.Name = "bunifuCustomLabel33";
            this.bunifuCustomLabel33.Size = new System.Drawing.Size(31, 13);
            this.bunifuCustomLabel33.TabIndex = 25;
            this.bunifuCustomLabel33.Text = "Tipo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtEmissaoUF);
            this.groupBox2.Controls.Add(this.dtEmissaoRg);
            this.groupBox2.Controls.Add(this.bunifuCustomLabel1);
            this.groupBox2.Controls.Add(this.bunifuCustomLabel18);
            this.groupBox2.Controls.Add(this.bunifuCustomLabel16);
            this.groupBox2.Controls.Add(this.txtRgNumero);
            this.groupBox2.Location = new System.Drawing.Point(10, 162);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 173);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Documento de identidade:";
            // 
            // txtEmissaoUF
            // 
            this.txtEmissaoUF.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmissaoUF.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmissaoUF.ForeColor = System.Drawing.Color.Black;
            this.txtEmissaoUF.HintForeColor = System.Drawing.Color.Black;
            this.txtEmissaoUF.HintText = "";
            this.txtEmissaoUF.isPassword = false;
            this.txtEmissaoUF.LineFocusedColor = System.Drawing.Color.Black;
            this.txtEmissaoUF.LineIdleColor = System.Drawing.Color.Gray;
            this.txtEmissaoUF.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtEmissaoUF.LineThickness = 2;
            this.txtEmissaoUF.Location = new System.Drawing.Point(88, 103);
            this.txtEmissaoUF.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmissaoUF.Name = "txtEmissaoUF";
            this.txtEmissaoUF.Size = new System.Drawing.Size(123, 28);
            this.txtEmissaoUF.TabIndex = 6;
            this.txtEmissaoUF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // dtEmissaoRg
            // 
            this.dtEmissaoRg.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissaoRg.Location = new System.Drawing.Point(75, 76);
            this.dtEmissaoRg.Name = "dtEmissaoRg";
            this.dtEmissaoRg.Size = new System.Drawing.Size(107, 20);
            this.dtEmissaoRg.TabIndex = 5;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(6, 82);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(49, 13);
            this.bunifuCustomLabel1.TabIndex = 31;
            this.bunifuCustomLabel1.Text = "Emissão:";
            // 
            // bunifuCustomLabel18
            // 
            this.bunifuCustomLabel18.AutoSize = true;
            this.bunifuCustomLabel18.Location = new System.Drawing.Point(6, 114);
            this.bunifuCustomLabel18.Name = "bunifuCustomLabel18";
            this.bunifuCustomLabel18.Size = new System.Drawing.Size(66, 13);
            this.bunifuCustomLabel18.TabIndex = 29;
            this.bunifuCustomLabel18.Text = "UF Emissão:";
            // 
            // bunifuCustomLabel16
            // 
            this.bunifuCustomLabel16.AutoSize = true;
            this.bunifuCustomLabel16.Location = new System.Drawing.Point(6, 51);
            this.bunifuCustomLabel16.Name = "bunifuCustomLabel16";
            this.bunifuCustomLabel16.Size = new System.Drawing.Size(66, 13);
            this.bunifuCustomLabel16.TabIndex = 25;
            this.bunifuCustomLabel16.Text = "Número RG:";
            // 
            // txtRgNumero
            // 
            this.txtRgNumero.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRgNumero.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRgNumero.ForeColor = System.Drawing.Color.Black;
            this.txtRgNumero.HintForeColor = System.Drawing.Color.Black;
            this.txtRgNumero.HintText = "";
            this.txtRgNumero.isPassword = false;
            this.txtRgNumero.LineFocusedColor = System.Drawing.Color.Black;
            this.txtRgNumero.LineIdleColor = System.Drawing.Color.Gray;
            this.txtRgNumero.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtRgNumero.LineThickness = 2;
            this.txtRgNumero.Location = new System.Drawing.Point(97, 40);
            this.txtRgNumero.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRgNumero.Name = "txtRgNumero";
            this.txtRgNumero.Size = new System.Drawing.Size(123, 28);
            this.txtRgNumero.TabIndex = 4;
            this.txtRgNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtUfEmissaoCpf);
            this.groupBox1.Controls.Add(this.bunifuCustomLabel15);
            this.groupBox1.Controls.Add(this.txtCpf);
            this.groupBox1.Controls.Add(this.dtEmissaoCpf);
            this.groupBox1.Controls.Add(this.bunifuCustomLabel19);
            this.groupBox1.Controls.Add(this.bunifuCustomLabel17);
            this.groupBox1.Location = new System.Drawing.Point(10, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 140);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CPF:";
            // 
            // txtUfEmissaoCpf
            // 
            this.txtUfEmissaoCpf.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUfEmissaoCpf.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUfEmissaoCpf.ForeColor = System.Drawing.Color.Black;
            this.txtUfEmissaoCpf.HintForeColor = System.Drawing.Color.Black;
            this.txtUfEmissaoCpf.HintText = "";
            this.txtUfEmissaoCpf.isPassword = false;
            this.txtUfEmissaoCpf.LineFocusedColor = System.Drawing.Color.Black;
            this.txtUfEmissaoCpf.LineIdleColor = System.Drawing.Color.Gray;
            this.txtUfEmissaoCpf.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtUfEmissaoCpf.LineThickness = 2;
            this.txtUfEmissaoCpf.Location = new System.Drawing.Point(93, 96);
            this.txtUfEmissaoCpf.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUfEmissaoCpf.Name = "txtUfEmissaoCpf";
            this.txtUfEmissaoCpf.Size = new System.Drawing.Size(123, 28);
            this.txtUfEmissaoCpf.TabIndex = 3;
            this.txtUfEmissaoCpf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel15
            // 
            this.bunifuCustomLabel15.AutoSize = true;
            this.bunifuCustomLabel15.Location = new System.Drawing.Point(6, 28);
            this.bunifuCustomLabel15.Name = "bunifuCustomLabel15";
            this.bunifuCustomLabel15.Size = new System.Drawing.Size(70, 13);
            this.bunifuCustomLabel15.TabIndex = 25;
            this.bunifuCustomLabel15.Text = "Número CPF:";
            // 
            // txtCpf
            // 
            this.txtCpf.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCpf.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCpf.ForeColor = System.Drawing.Color.Black;
            this.txtCpf.HintForeColor = System.Drawing.Color.Black;
            this.txtCpf.HintText = "";
            this.txtCpf.isPassword = false;
            this.txtCpf.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCpf.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCpf.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCpf.LineThickness = 2;
            this.txtCpf.Location = new System.Drawing.Point(97, 17);
            this.txtCpf.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(114, 28);
            this.txtCpf.TabIndex = 1;
            this.txtCpf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // dtEmissaoCpf
            // 
            this.dtEmissaoCpf.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissaoCpf.Location = new System.Drawing.Point(81, 62);
            this.dtEmissaoCpf.Name = "dtEmissaoCpf";
            this.dtEmissaoCpf.Size = new System.Drawing.Size(107, 20);
            this.dtEmissaoCpf.TabIndex = 2;
            // 
            // bunifuCustomLabel19
            // 
            this.bunifuCustomLabel19.AutoSize = true;
            this.bunifuCustomLabel19.Location = new System.Drawing.Point(12, 107);
            this.bunifuCustomLabel19.Name = "bunifuCustomLabel19";
            this.bunifuCustomLabel19.Size = new System.Drawing.Size(66, 13);
            this.bunifuCustomLabel19.TabIndex = 29;
            this.bunifuCustomLabel19.Text = "UF Emissão:";
            // 
            // bunifuCustomLabel17
            // 
            this.bunifuCustomLabel17.AutoSize = true;
            this.bunifuCustomLabel17.Location = new System.Drawing.Point(12, 68);
            this.bunifuCustomLabel17.Name = "bunifuCustomLabel17";
            this.bunifuCustomLabel17.Size = new System.Drawing.Size(49, 13);
            this.bunifuCustomLabel17.TabIndex = 27;
            this.bunifuCustomLabel17.Text = "Emissão:";
            // 
            // tabPageEX3
            // 
            this.tabPageEX3.Controls.Add(this.groupBoxEX5);
            this.tabPageEX3.Location = new System.Drawing.Point(4, 25);
            this.tabPageEX3.Name = "tabPageEX3";
            this.tabPageEX3.Size = new System.Drawing.Size(1020, 457);
            this.tabPageEX3.TabIndex = 2;
            this.tabPageEX3.Text = "Informações Funcionais";
            // 
            // groupBoxEX5
            // 
            this.groupBoxEX5.Controls.Add(this.groupBox8);
            this.groupBoxEX5.Controls.Add(this.groupBox7);
            this.groupBoxEX5.Controls.Add(this.groupBox6);
            this.groupBoxEX5.Controls.Add(this.groupBox5);
            this.groupBoxEX5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEX5.Location = new System.Drawing.Point(0, 0);
            this.groupBoxEX5.Name = "groupBoxEX5";
            this.groupBoxEX5.Size = new System.Drawing.Size(1020, 690);
            this.groupBoxEX5.TabIndex = 2;
            this.groupBoxEX5.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtBanco);
            this.groupBox8.Controls.Add(this.txtAgencia);
            this.groupBox8.Controls.Add(this.bunifuCustomLabel50);
            this.groupBox8.Controls.Add(this.txtConta);
            this.groupBox8.Controls.Add(this.bunifuCustomLabel49);
            this.groupBox8.Controls.Add(this.bunifuCustomLabel48);
            this.groupBox8.Location = new System.Drawing.Point(311, 149);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(366, 153);
            this.groupBox8.TabIndex = 41;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Dados bancarios";
            // 
            // txtBanco
            // 
            this.txtBanco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBanco.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBanco.ForeColor = System.Drawing.Color.Black;
            this.txtBanco.HintForeColor = System.Drawing.Color.Black;
            this.txtBanco.HintText = "";
            this.txtBanco.isPassword = false;
            this.txtBanco.LineFocusedColor = System.Drawing.Color.Black;
            this.txtBanco.LineIdleColor = System.Drawing.Color.Gray;
            this.txtBanco.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtBanco.LineThickness = 2;
            this.txtBanco.Location = new System.Drawing.Point(73, 24);
            this.txtBanco.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBanco.Name = "txtBanco";
            this.txtBanco.Size = new System.Drawing.Size(132, 28);
            this.txtBanco.TabIndex = 6;
            this.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtAgencia
            // 
            this.txtAgencia.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtAgencia.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAgencia.ForeColor = System.Drawing.Color.Black;
            this.txtAgencia.HintForeColor = System.Drawing.Color.Black;
            this.txtAgencia.HintText = "";
            this.txtAgencia.isPassword = false;
            this.txtAgencia.LineFocusedColor = System.Drawing.Color.Black;
            this.txtAgencia.LineIdleColor = System.Drawing.Color.Gray;
            this.txtAgencia.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtAgencia.LineThickness = 2;
            this.txtAgencia.Location = new System.Drawing.Point(141, 98);
            this.txtAgencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAgencia.Name = "txtAgencia";
            this.txtAgencia.Size = new System.Drawing.Size(132, 28);
            this.txtAgencia.TabIndex = 8;
            this.txtAgencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel50
            // 
            this.bunifuCustomLabel50.AutoSize = true;
            this.bunifuCustomLabel50.Location = new System.Drawing.Point(15, 105);
            this.bunifuCustomLabel50.Name = "bunifuCustomLabel50";
            this.bunifuCustomLabel50.Size = new System.Drawing.Size(49, 13);
            this.bunifuCustomLabel50.TabIndex = 43;
            this.bunifuCustomLabel50.Text = "Agência:";
            // 
            // txtConta
            // 
            this.txtConta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtConta.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConta.ForeColor = System.Drawing.Color.Black;
            this.txtConta.HintForeColor = System.Drawing.Color.Black;
            this.txtConta.HintText = "";
            this.txtConta.isPassword = false;
            this.txtConta.LineFocusedColor = System.Drawing.Color.Black;
            this.txtConta.LineIdleColor = System.Drawing.Color.Gray;
            this.txtConta.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtConta.LineThickness = 2;
            this.txtConta.Location = new System.Drawing.Point(141, 64);
            this.txtConta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(132, 28);
            this.txtConta.TabIndex = 7;
            this.txtConta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel49
            // 
            this.bunifuCustomLabel49.AutoSize = true;
            this.bunifuCustomLabel49.Location = new System.Drawing.Point(15, 71);
            this.bunifuCustomLabel49.Name = "bunifuCustomLabel49";
            this.bunifuCustomLabel49.Size = new System.Drawing.Size(53, 13);
            this.bunifuCustomLabel49.TabIndex = 41;
            this.bunifuCustomLabel49.Text = "Nº Conta:";
            // 
            // bunifuCustomLabel48
            // 
            this.bunifuCustomLabel48.AutoSize = true;
            this.bunifuCustomLabel48.Location = new System.Drawing.Point(13, 35);
            this.bunifuCustomLabel48.Name = "bunifuCustomLabel48";
            this.bunifuCustomLabel48.Size = new System.Drawing.Size(41, 13);
            this.bunifuCustomLabel48.TabIndex = 39;
            this.bunifuCustomLabel48.Text = "Banco:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtSalario);
            this.groupBox7.Controls.Add(this.bunifuCustomLabel46);
            this.groupBox7.Location = new System.Drawing.Point(6, 185);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(299, 78);
            this.groupBox7.TabIndex = 36;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Salário";
            // 
            // txtSalario
            // 
            this.txtSalario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtSalario.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalario.ForeColor = System.Drawing.Color.Black;
            this.txtSalario.HintForeColor = System.Drawing.Color.Black;
            this.txtSalario.HintText = "";
            this.txtSalario.isPassword = false;
            this.txtSalario.LineFocusedColor = System.Drawing.Color.Black;
            this.txtSalario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtSalario.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtSalario.LineThickness = 2;
            this.txtSalario.Location = new System.Drawing.Point(132, 28);
            this.txtSalario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.Size = new System.Drawing.Size(92, 28);
            this.txtSalario.TabIndex = 3;
            this.txtSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel46
            // 
            this.bunifuCustomLabel46.AutoSize = true;
            this.bunifuCustomLabel46.Location = new System.Drawing.Point(6, 35);
            this.bunifuCustomLabel46.Name = "bunifuCustomLabel46";
            this.bunifuCustomLabel46.Size = new System.Drawing.Size(75, 13);
            this.bunifuCustomLabel46.TabIndex = 0;
            this.bunifuCustomLabel46.Text = "Salário (bruto):";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtsenha);
            this.groupBox6.Controls.Add(this.bunifuCustomLabel20);
            this.groupBox6.Controls.Add(this.txtusuario);
            this.groupBox6.Controls.Add(this.bunifuCustomLabel4);
            this.groupBox6.Controls.Add(this.txtCargo);
            this.groupBox6.Controls.Add(this.bunifuCustomLabel31);
            this.groupBox6.Controls.Add(this.txtDepartamento);
            this.groupBox6.Controls.Add(this.bunifuCustomLabel32);
            this.groupBox6.Location = new System.Drawing.Point(311, 26);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(366, 117);
            this.groupBox6.TabIndex = 36;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Trabalhador";
            // 
            // txtsenha
            // 
            this.txtsenha.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtsenha.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.ForeColor = System.Drawing.Color.Black;
            this.txtsenha.HintForeColor = System.Drawing.Color.Black;
            this.txtsenha.HintText = "";
            this.txtsenha.isPassword = false;
            this.txtsenha.LineFocusedColor = System.Drawing.Color.Black;
            this.txtsenha.LineIdleColor = System.Drawing.Color.Gray;
            this.txtsenha.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtsenha.LineThickness = 2;
            this.txtsenha.Location = new System.Drawing.Point(217, 72);
            this.txtsenha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(142, 28);
            this.txtsenha.TabIndex = 30;
            this.txtsenha.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel20
            // 
            this.bunifuCustomLabel20.AutoSize = true;
            this.bunifuCustomLabel20.Location = new System.Drawing.Point(254, 57);
            this.bunifuCustomLabel20.Name = "bunifuCustomLabel20";
            this.bunifuCustomLabel20.Size = new System.Drawing.Size(41, 13);
            this.bunifuCustomLabel20.TabIndex = 31;
            this.bunifuCustomLabel20.Text = "Senha:";
            // 
            // txtusuario
            // 
            this.txtusuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtusuario.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtusuario.ForeColor = System.Drawing.Color.Black;
            this.txtusuario.HintForeColor = System.Drawing.Color.Black;
            this.txtusuario.HintText = "";
            this.txtusuario.isPassword = false;
            this.txtusuario.LineFocusedColor = System.Drawing.Color.Black;
            this.txtusuario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtusuario.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtusuario.LineThickness = 2;
            this.txtusuario.Location = new System.Drawing.Point(62, 81);
            this.txtusuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(132, 28);
            this.txtusuario.TabIndex = 28;
            this.txtusuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(6, 92);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(46, 13);
            this.bunifuCustomLabel4.TabIndex = 29;
            this.bunifuCustomLabel4.Text = "Usuario:";
            // 
            // txtCargo
            // 
            this.txtCargo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCargo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCargo.ForeColor = System.Drawing.Color.Black;
            this.txtCargo.HintForeColor = System.Drawing.Color.Black;
            this.txtCargo.HintText = "";
            this.txtCargo.isPassword = false;
            this.txtCargo.LineFocusedColor = System.Drawing.Color.Black;
            this.txtCargo.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCargo.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtCargo.LineThickness = 2;
            this.txtCargo.Location = new System.Drawing.Point(73, 46);
            this.txtCargo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.Size = new System.Drawing.Size(132, 28);
            this.txtCargo.TabIndex = 5;
            this.txtCargo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel31
            // 
            this.bunifuCustomLabel31.AutoSize = true;
            this.bunifuCustomLabel31.Location = new System.Drawing.Point(6, 57);
            this.bunifuCustomLabel31.Name = "bunifuCustomLabel31";
            this.bunifuCustomLabel31.Size = new System.Drawing.Size(46, 13);
            this.bunifuCustomLabel31.TabIndex = 27;
            this.bunifuCustomLabel31.Text = "Função:";
            // 
            // txtDepartamento
            // 
            this.txtDepartamento.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDepartamento.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepartamento.ForeColor = System.Drawing.Color.Black;
            this.txtDepartamento.HintForeColor = System.Drawing.Color.Black;
            this.txtDepartamento.HintText = "";
            this.txtDepartamento.isPassword = false;
            this.txtDepartamento.LineFocusedColor = System.Drawing.Color.Black;
            this.txtDepartamento.LineIdleColor = System.Drawing.Color.Gray;
            this.txtDepartamento.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtDepartamento.LineThickness = 2;
            this.txtDepartamento.Location = new System.Drawing.Point(122, 12);
            this.txtDepartamento.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDepartamento.Name = "txtDepartamento";
            this.txtDepartamento.Size = new System.Drawing.Size(132, 28);
            this.txtDepartamento.TabIndex = 4;
            this.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel32
            // 
            this.bunifuCustomLabel32.AutoSize = true;
            this.bunifuCustomLabel32.Location = new System.Drawing.Point(6, 23);
            this.bunifuCustomLabel32.Name = "bunifuCustomLabel32";
            this.bunifuCustomLabel32.Size = new System.Drawing.Size(77, 13);
            this.bunifuCustomLabel32.TabIndex = 0;
            this.bunifuCustomLabel32.Text = "Departamento:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dateTimePicker6);
            this.groupBox5.Controls.Add(this.bunifuCustomLabel43);
            this.groupBox5.Controls.Add(this.dtContrato);
            this.groupBox5.Controls.Add(this.bunifuCustomLabel30);
            this.groupBox5.Location = new System.Drawing.Point(4, 23);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(301, 156);
            this.groupBox5.TabIndex = 34;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Trabalhador";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.dateTimePicker6.Location = new System.Drawing.Point(159, 80);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(113, 20);
            this.dateTimePicker6.TabIndex = 2;
            // 
            // bunifuCustomLabel43
            // 
            this.bunifuCustomLabel43.AutoSize = true;
            this.bunifuCustomLabel43.Location = new System.Drawing.Point(5, 86);
            this.bunifuCustomLabel43.Name = "bunifuCustomLabel43";
            this.bunifuCustomLabel43.Size = new System.Drawing.Size(83, 13);
            this.bunifuCustomLabel43.TabIndex = 35;
            this.bunifuCustomLabel43.Text = "Fim de contrato:";
            // 
            // dtContrato
            // 
            this.dtContrato.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtContrato.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtContrato.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.dtContrato.Location = new System.Drawing.Point(182, 35);
            this.dtContrato.Name = "dtContrato";
            this.dtContrato.Size = new System.Drawing.Size(113, 20);
            this.dtContrato.TabIndex = 1;
            // 
            // bunifuCustomLabel30
            // 
            this.bunifuCustomLabel30.AutoSize = true;
            this.bunifuCustomLabel30.Location = new System.Drawing.Point(5, 41);
            this.bunifuCustomLabel30.Name = "bunifuCustomLabel30";
            this.bunifuCustomLabel30.Size = new System.Drawing.Size(95, 13);
            this.bunifuCustomLabel30.TabIndex = 27;
            this.bunifuCustomLabel30.Text = "Data de admissão:";
            // 
            // frmRegistrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 720);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.tabControlEX1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmRegistrarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRegistrarFuncionario";
            this.tabControlEX1.ResumeLayout(false);
            this.tabPageEX1.ResumeLayout(false);
            this.groupBoxEX2.ResumeLayout(false);
            this.groupBoxEX2.PerformLayout();
            this.groupBoxEX1.ResumeLayout(false);
            this.groupBoxEX1.PerformLayout();
            this.tabPageEX2.ResumeLayout(false);
            this.groupBoxEX4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageEX3.ResumeLayout(false);
            this.groupBoxEX5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton btnSalvar;
        private Dotnetrix.Controls.TabControlEX tabControlEX1;
        private Dotnetrix.Controls.TabPageEX tabPageEX1;
        private Dotnetrix.Controls.GroupBoxEX groupBoxEX2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtTelefone;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCidade;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtEstado;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtPais;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel13;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCivilEstado;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCelular;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel41;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel39;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtComplemento;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel38;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtNumero;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel37;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel36;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtEndereço;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCep;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel34;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel14;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel28;
        private Dotnetrix.Controls.GroupBoxEX groupBoxEX1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtsobrenome;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel21;
        private System.Windows.Forms.ComboBox cboSexo;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtDescDeficiencia;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtRaca;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel11;
        private System.Windows.Forms.RadioButton rdbDS;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel10;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel9;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel8;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private System.Windows.Forms.DateTimePicker dtNasc;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtnomefuncionario;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Dotnetrix.Controls.TabPageEX tabPageEX2;
        private Dotnetrix.Controls.GroupBoxEX groupBoxEX4;
        private System.Windows.Forms.GroupBox groupBox4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtUfPiss;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtPisNumero;
        private System.Windows.Forms.ComboBox cboTypePis;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel26;
        private System.Windows.Forms.DateTimePicker dtEmissaoPis;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel27;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel33;
        private System.Windows.Forms.GroupBox groupBox2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtEmissaoUF;
        private System.Windows.Forms.DateTimePicker dtEmissaoRg;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel18;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel16;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtRgNumero;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtUfEmissaoCpf;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCpf;
        private System.Windows.Forms.DateTimePicker dtEmissaoCpf;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel19;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel17;
        private Dotnetrix.Controls.TabPageEX tabPageEX3;
        private Dotnetrix.Controls.GroupBoxEX groupBoxEX5;
        private System.Windows.Forms.GroupBox groupBox8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtBanco;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtAgencia;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel50;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtConta;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel49;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel48;
        private System.Windows.Forms.GroupBox groupBox7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtSalario;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel46;
        private System.Windows.Forms.GroupBox groupBox6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtsenha;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel20;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtusuario;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCargo;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel31;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtDepartamento;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel32;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel43;
        private System.Windows.Forms.DateTimePicker dtContrato;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel30;
    }
}