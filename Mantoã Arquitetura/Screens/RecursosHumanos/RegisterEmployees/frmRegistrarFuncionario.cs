﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantoã_Arquitetura.Screens.RecursosHumanos.RegisterEmployees
{
    public partial class frmRegistrarFuncionario : Form
    {
        public frmRegistrarFuncionario()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //Instancias
            Model.Entities.tb_employees dados = new Model.Entities.tb_employees();
            Model.Entities.tb_enderecofuncionario endereco = new Model.Entities.tb_enderecofuncionario();
            Model.Entities.tb_documentofuncionario Doc = new Model.Entities.tb_documentofuncionario();
            Model.Entities.tb_bancofuncionario Banco = new Model.Entities.tb_bancofuncionario();
            Model.Entities.tb_salariofuncionario salario = new Model.Entities.tb_salariofuncionario();
            Model.Entities.tb_cargo cargo = new Model.Entities.tb_cargo();
            Model.Entities.tb_departamento dept = new Model.Entities.tb_departamento();
            Model.Entities.tb_contafuncionario conta = new Model.Entities.tb_contafuncionario();
            Business.FuncionarioBancoBusiness bsFuncionarioBanco = new Business.FuncionarioBancoBusiness();
            Business.FuncionarioBusiness bsFuncionario = new Business.FuncionarioBusiness();
            Business.FuncionarioDocBusiness bsFuncionarioDoc = new Business.FuncionarioDocBusiness();
            Business.FuncionarioEndBusiness bsFuncionarioEnd = new Business.FuncionarioEndBusiness();
            Business.FuncionarioSalarioBusiness bsFuncionarioSalario = new Business.FuncionarioSalarioBusiness();
            Business.FuncionarioContaBusiness bsFuncionarioConta = new Business.FuncionarioContaBusiness();

            //Coletanto dados do funcionário
            dados.nm_nome = txtnomefuncionario.Text;
            dados.nm_sobrenome = txtsobrenome.Text;
            dados.dt_nascimento = dtNasc.Value;
            dados.ds_sexo = Convert.ToString(cboSexo.SelectedItem);
            dados.dt_contratado = dtContrato.Value;
            dados.ds_etnia = txtRaca.Text;
            dados.ds_deficiencia = rdbDS.Checked;
            dados.ds_deficiencianotas = txtDescDeficiencia.Text;
            dados.ds_estadocivil = txtCivilEstado.Text;
            dados.nr_telefone = txtTelefone.Text;
            dados.nr_celular = txtCelular.Text;
            //Colentando dados do endereço
            endereco.nr_cep = txtCep.Text;
            endereco.nm_pais = txtPais.Text;
            endereco.nm_estado = txtEstado.Text;
            endereco.nm_cidade = txtCidade.Text;
            endereco.ds_endereco = txtEndereço.Text;
            endereco.nr_numero = txtNumero.Text;
            //Coletando Documentos 
            Doc.ds_cpf = Convert.ToString(txtCpf.Text);
            Doc.ds_cpfuf = Convert.ToString(txtUfEmissaoCpf.Text);
            Doc.dt_emissaocpf = dtEmissaoCpf.Value;
            Doc.ds_rg = Convert.ToString(txtRgNumero.Text);
            Doc.ds_rguf = Convert.ToString(txtEmissaoUF.Text);
            Doc.ds_emissaorg = dtEmissaoRg.Value;
            Doc.ds_pisnumero = Convert.ToString(txtPisNumero.Text);
            Doc.ds_pisuf = Convert.ToString(txtUfPiss.Text);
            Doc.dt_pisemissao = dtEmissaoPis.Value;
            Doc.ds_pistipo = cboTypePis.Text;
            //Coletando dados funcionais
            dados.dt_contratado = dtContrato.Value;
            salario.dt_data = dtContrato.Value;
            salario.vl_salario = Convert.ToInt32(txtSalario.Text);
            cargo.nm_cargo = txtCargo.Text;
            dept.nm_departamento = txtDepartamento.Text;
            Banco.cd_agencia = txtAgencia.Text;
            Banco.nm_banco = txtBanco.Text;
            Banco.cd_contaCorrente = txtConta.Text;
            conta.nm_usuario = txtusuario.Text;
            conta.pw_senha = txtsenha.Text;
            //Inserindo o funcionário
            bsFuncionario.ValidarFuncionario(dados);

            //definindo chaves estrangeiras
            endereco.id_emp = dados.id_emp;
            Doc.id_documentofun = dados.id_emp;
            salario.id_salariofuncionario = dados.id_emp;
            Banco.id_bancofuncionario = dados.id_emp;
            conta.id_contafuncionario = dados.id_emp;

            //inserindo com chave estrangeira
            bsFuncionarioSalario.FuncionarioSalarioValidar(salario);
            bsFuncionarioDoc.ValidarFuncionarioDoc(Doc);
            bsFuncionarioBanco.ValidarFuncionarioBanco(Banco);
            bsFuncionarioEnd.ValidarFuncionarioEnd(endereco);
            bsFuncionarioConta.ValidarContaFuncionario(conta);

        }
    }
}
