﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantoã_Arquitetura.Screens.UserLogin
{
    public partial class frmlogin : Form
    {
        public frmlogin()
        {
            InitializeComponent();
        }

        public void validatelogin()
        {

            string user = txtUser.Text;
            string pass = txtPass.Text;

            Business.FuncionarioContaBusiness validarlogin = new Business.FuncionarioContaBusiness();

            Model.Entities.tb_contafuncionario conta = validarlogin.ReceberLogin(user, pass);

            if(conta != null)
            {
                Screens.Dashboard.frmHome tela = new Dashboard.frmHome();
                tela.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Login errado");
            }

        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            validatelogin();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
