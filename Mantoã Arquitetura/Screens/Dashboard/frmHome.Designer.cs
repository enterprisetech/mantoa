﻿namespace Mantoã_Arquitetura.Screens.Dashboard
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.pnlContainer2 = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlFinanceiro = new System.Windows.Forms.Panel();
            this.btnPagamentosEfetuados = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnPagamentosRecebidos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnPainelDeControle = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnFinanceiro = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnlRH = new System.Windows.Forms.Panel();
            this.btnListEmp = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRegisterEmp = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRH = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnHome = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlBar = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlContainer.SuspendLayout();
            this.pnlContainer2.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.pnlFinanceiro.SuspendLayout();
            this.pnlRH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.Color.White;
            this.pnlContainer.Controls.Add(this.pnlContainer2);
            this.pnlContainer.Controls.Add(this.pnlBar);
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Margin = new System.Windows.Forms.Padding(4);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(1028, 599);
            this.pnlContainer.TabIndex = 0;
            // 
            // pnlContainer2
            // 
            this.pnlContainer2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.pnlContainer2.Controls.Add(this.pnlMenu);
            this.pnlContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer2.Location = new System.Drawing.Point(0, 28);
            this.pnlContainer2.Margin = new System.Windows.Forms.Padding(4);
            this.pnlContainer2.MinimumSize = new System.Drawing.Size(1133, 555);
            this.pnlContainer2.Name = "pnlContainer2";
            this.pnlContainer2.Size = new System.Drawing.Size(1133, 571);
            this.pnlContainer2.TabIndex = 2;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.White;
            this.pnlMenu.Controls.Add(this.pnlFinanceiro);
            this.pnlMenu.Controls.Add(this.btnFinanceiro);
            this.pnlMenu.Controls.Add(this.pnlRH);
            this.pnlMenu.Controls.Add(this.btnRH);
            this.pnlMenu.Controls.Add(this.btnHome);
            this.pnlMenu.Controls.Add(this.pictureBox1);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(244, 571);
            this.pnlMenu.TabIndex = 0;
            // 
            // pnlFinanceiro
            // 
            this.pnlFinanceiro.BackColor = System.Drawing.Color.Transparent;
            this.pnlFinanceiro.Controls.Add(this.btnPagamentosEfetuados);
            this.pnlFinanceiro.Controls.Add(this.btnPagamentosRecebidos);
            this.pnlFinanceiro.Controls.Add(this.btnPainelDeControle);
            this.pnlFinanceiro.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFinanceiro.Location = new System.Drawing.Point(0, 478);
            this.pnlFinanceiro.Name = "pnlFinanceiro";
            this.pnlFinanceiro.Size = new System.Drawing.Size(227, 210);
            this.pnlFinanceiro.TabIndex = 5;
            this.pnlFinanceiro.Visible = false;
            // 
            // btnPagamentosEfetuados
            // 
            this.btnPagamentosEfetuados.Activecolor = System.Drawing.Color.Transparent;
            this.btnPagamentosEfetuados.BackColor = System.Drawing.Color.Transparent;
            this.btnPagamentosEfetuados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPagamentosEfetuados.BorderRadius = 0;
            this.btnPagamentosEfetuados.ButtonText = "Efetuar pagamento";
            this.btnPagamentosEfetuados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPagamentosEfetuados.DisabledColor = System.Drawing.Color.Gray;
            this.btnPagamentosEfetuados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPagamentosEfetuados.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagamentosEfetuados.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPagamentosEfetuados.Iconimage = null;
            this.btnPagamentosEfetuados.Iconimage_right = null;
            this.btnPagamentosEfetuados.Iconimage_right_Selected = null;
            this.btnPagamentosEfetuados.Iconimage_Selected = null;
            this.btnPagamentosEfetuados.IconMarginLeft = 0;
            this.btnPagamentosEfetuados.IconMarginRight = 0;
            this.btnPagamentosEfetuados.IconRightVisible = true;
            this.btnPagamentosEfetuados.IconRightZoom = 0D;
            this.btnPagamentosEfetuados.IconVisible = true;
            this.btnPagamentosEfetuados.IconZoom = 40D;
            this.btnPagamentosEfetuados.IsTab = false;
            this.btnPagamentosEfetuados.Location = new System.Drawing.Point(0, 124);
            this.btnPagamentosEfetuados.Margin = new System.Windows.Forms.Padding(5);
            this.btnPagamentosEfetuados.Name = "btnPagamentosEfetuados";
            this.btnPagamentosEfetuados.Normalcolor = System.Drawing.Color.Transparent;
            this.btnPagamentosEfetuados.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnPagamentosEfetuados.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnPagamentosEfetuados.selected = false;
            this.btnPagamentosEfetuados.Size = new System.Drawing.Size(227, 59);
            this.btnPagamentosEfetuados.TabIndex = 5;
            this.btnPagamentosEfetuados.Text = "Efetuar pagamento";
            this.btnPagamentosEfetuados.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPagamentosEfetuados.Textcolor = System.Drawing.Color.Black;
            this.btnPagamentosEfetuados.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagamentosEfetuados.Click += new System.EventHandler(this.btnPagamentosEfetuados_Click);
            // 
            // btnPagamentosRecebidos
            // 
            this.btnPagamentosRecebidos.Activecolor = System.Drawing.Color.Transparent;
            this.btnPagamentosRecebidos.BackColor = System.Drawing.Color.Transparent;
            this.btnPagamentosRecebidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPagamentosRecebidos.BorderRadius = 0;
            this.btnPagamentosRecebidos.ButtonText = "Receber pagamento";
            this.btnPagamentosRecebidos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPagamentosRecebidos.DisabledColor = System.Drawing.Color.Gray;
            this.btnPagamentosRecebidos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPagamentosRecebidos.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagamentosRecebidos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPagamentosRecebidos.Iconimage = null;
            this.btnPagamentosRecebidos.Iconimage_right = null;
            this.btnPagamentosRecebidos.Iconimage_right_Selected = null;
            this.btnPagamentosRecebidos.Iconimage_Selected = null;
            this.btnPagamentosRecebidos.IconMarginLeft = 0;
            this.btnPagamentosRecebidos.IconMarginRight = 0;
            this.btnPagamentosRecebidos.IconRightVisible = true;
            this.btnPagamentosRecebidos.IconRightZoom = 0D;
            this.btnPagamentosRecebidos.IconVisible = true;
            this.btnPagamentosRecebidos.IconZoom = 40D;
            this.btnPagamentosRecebidos.IsTab = false;
            this.btnPagamentosRecebidos.Location = new System.Drawing.Point(0, 62);
            this.btnPagamentosRecebidos.Margin = new System.Windows.Forms.Padding(5);
            this.btnPagamentosRecebidos.Name = "btnPagamentosRecebidos";
            this.btnPagamentosRecebidos.Normalcolor = System.Drawing.Color.Transparent;
            this.btnPagamentosRecebidos.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnPagamentosRecebidos.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnPagamentosRecebidos.selected = false;
            this.btnPagamentosRecebidos.Size = new System.Drawing.Size(227, 62);
            this.btnPagamentosRecebidos.TabIndex = 4;
            this.btnPagamentosRecebidos.Text = "Receber pagamento";
            this.btnPagamentosRecebidos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPagamentosRecebidos.Textcolor = System.Drawing.Color.Black;
            this.btnPagamentosRecebidos.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagamentosRecebidos.Click += new System.EventHandler(this.btnPagamentosRecebidos_Click);
            // 
            // btnPainelDeControle
            // 
            this.btnPainelDeControle.Activecolor = System.Drawing.Color.Transparent;
            this.btnPainelDeControle.BackColor = System.Drawing.Color.Transparent;
            this.btnPainelDeControle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPainelDeControle.BorderRadius = 0;
            this.btnPainelDeControle.ButtonText = "Painel de controle";
            this.btnPainelDeControle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPainelDeControle.DisabledColor = System.Drawing.Color.Gray;
            this.btnPainelDeControle.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPainelDeControle.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPainelDeControle.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPainelDeControle.Iconimage = null;
            this.btnPainelDeControle.Iconimage_right = null;
            this.btnPainelDeControle.Iconimage_right_Selected = null;
            this.btnPainelDeControle.Iconimage_Selected = null;
            this.btnPainelDeControle.IconMarginLeft = 0;
            this.btnPainelDeControle.IconMarginRight = 0;
            this.btnPainelDeControle.IconRightVisible = true;
            this.btnPainelDeControle.IconRightZoom = 0D;
            this.btnPainelDeControle.IconVisible = true;
            this.btnPainelDeControle.IconZoom = 40D;
            this.btnPainelDeControle.IsTab = false;
            this.btnPainelDeControle.Location = new System.Drawing.Point(0, 0);
            this.btnPainelDeControle.Margin = new System.Windows.Forms.Padding(5);
            this.btnPainelDeControle.Name = "btnPainelDeControle";
            this.btnPainelDeControle.Normalcolor = System.Drawing.Color.Transparent;
            this.btnPainelDeControle.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnPainelDeControle.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnPainelDeControle.selected = false;
            this.btnPainelDeControle.Size = new System.Drawing.Size(227, 62);
            this.btnPainelDeControle.TabIndex = 4;
            this.btnPainelDeControle.Text = "Painel de controle";
            this.btnPainelDeControle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPainelDeControle.Textcolor = System.Drawing.Color.Black;
            this.btnPainelDeControle.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPainelDeControle.Click += new System.EventHandler(this.btnPainelDeControle_Click);
            // 
            // btnFinanceiro
            // 
            this.btnFinanceiro.Activecolor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.BackColor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFinanceiro.BorderRadius = 0;
            this.btnFinanceiro.ButtonText = "Financeiro";
            this.btnFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinanceiro.DisabledColor = System.Drawing.Color.Gray;
            this.btnFinanceiro.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFinanceiro.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinanceiro.Iconcolor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnFinanceiro.Iconimage")));
            this.btnFinanceiro.Iconimage_right = ((System.Drawing.Image)(resources.GetObject("btnFinanceiro.Iconimage_right")));
            this.btnFinanceiro.Iconimage_right_Selected = null;
            this.btnFinanceiro.Iconimage_Selected = null;
            this.btnFinanceiro.IconMarginLeft = 0;
            this.btnFinanceiro.IconMarginRight = 0;
            this.btnFinanceiro.IconRightVisible = true;
            this.btnFinanceiro.IconRightZoom = 0D;
            this.btnFinanceiro.IconVisible = true;
            this.btnFinanceiro.IconZoom = 40D;
            this.btnFinanceiro.IsTab = false;
            this.btnFinanceiro.Location = new System.Drawing.Point(0, 424);
            this.btnFinanceiro.Margin = new System.Windows.Forms.Padding(5);
            this.btnFinanceiro.Name = "btnFinanceiro";
            this.btnFinanceiro.Normalcolor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnFinanceiro.selected = false;
            this.btnFinanceiro.Size = new System.Drawing.Size(227, 54);
            this.btnFinanceiro.TabIndex = 4;
            this.btnFinanceiro.Text = "Financeiro";
            this.btnFinanceiro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnFinanceiro.Textcolor = System.Drawing.Color.Black;
            this.btnFinanceiro.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinanceiro.Click += new System.EventHandler(this.btnFinanceiro_Click);
            // 
            // pnlRH
            // 
            this.pnlRH.BackColor = System.Drawing.Color.Transparent;
            this.pnlRH.Controls.Add(this.btnListEmp);
            this.pnlRH.Controls.Add(this.btnRegisterEmp);
            this.pnlRH.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRH.Location = new System.Drawing.Point(0, 279);
            this.pnlRH.Name = "pnlRH";
            this.pnlRH.Size = new System.Drawing.Size(227, 145);
            this.pnlRH.TabIndex = 3;
            this.pnlRH.Visible = false;
            // 
            // btnListEmp
            // 
            this.btnListEmp.Activecolor = System.Drawing.Color.Transparent;
            this.btnListEmp.BackColor = System.Drawing.Color.Transparent;
            this.btnListEmp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnListEmp.BorderRadius = 0;
            this.btnListEmp.ButtonText = "Consultar Funcionário";
            this.btnListEmp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListEmp.DisabledColor = System.Drawing.Color.Gray;
            this.btnListEmp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListEmp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListEmp.Iconcolor = System.Drawing.Color.Transparent;
            this.btnListEmp.Iconimage = null;
            this.btnListEmp.Iconimage_right = null;
            this.btnListEmp.Iconimage_right_Selected = null;
            this.btnListEmp.Iconimage_Selected = null;
            this.btnListEmp.IconMarginLeft = 0;
            this.btnListEmp.IconMarginRight = 0;
            this.btnListEmp.IconRightVisible = true;
            this.btnListEmp.IconRightZoom = 0D;
            this.btnListEmp.IconVisible = true;
            this.btnListEmp.IconZoom = 40D;
            this.btnListEmp.IsTab = false;
            this.btnListEmp.Location = new System.Drawing.Point(0, 62);
            this.btnListEmp.Margin = new System.Windows.Forms.Padding(5);
            this.btnListEmp.Name = "btnListEmp";
            this.btnListEmp.Normalcolor = System.Drawing.Color.Transparent;
            this.btnListEmp.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnListEmp.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnListEmp.selected = false;
            this.btnListEmp.Size = new System.Drawing.Size(227, 62);
            this.btnListEmp.TabIndex = 4;
            this.btnListEmp.Text = "Consultar Funcionário";
            this.btnListEmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnListEmp.Textcolor = System.Drawing.Color.Black;
            this.btnListEmp.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListEmp.Click += new System.EventHandler(this.btnListEmp_Click);
            // 
            // btnRegisterEmp
            // 
            this.btnRegisterEmp.Activecolor = System.Drawing.Color.Transparent;
            this.btnRegisterEmp.BackColor = System.Drawing.Color.Transparent;
            this.btnRegisterEmp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegisterEmp.BorderRadius = 0;
            this.btnRegisterEmp.ButtonText = "Cadastrar Funcionário";
            this.btnRegisterEmp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegisterEmp.DisabledColor = System.Drawing.Color.Gray;
            this.btnRegisterEmp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegisterEmp.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterEmp.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRegisterEmp.Iconimage = null;
            this.btnRegisterEmp.Iconimage_right = null;
            this.btnRegisterEmp.Iconimage_right_Selected = null;
            this.btnRegisterEmp.Iconimage_Selected = null;
            this.btnRegisterEmp.IconMarginLeft = 0;
            this.btnRegisterEmp.IconMarginRight = 0;
            this.btnRegisterEmp.IconRightVisible = true;
            this.btnRegisterEmp.IconRightZoom = 0D;
            this.btnRegisterEmp.IconVisible = true;
            this.btnRegisterEmp.IconZoom = 40D;
            this.btnRegisterEmp.IsTab = false;
            this.btnRegisterEmp.Location = new System.Drawing.Point(0, 0);
            this.btnRegisterEmp.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegisterEmp.Name = "btnRegisterEmp";
            this.btnRegisterEmp.Normalcolor = System.Drawing.Color.Transparent;
            this.btnRegisterEmp.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnRegisterEmp.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnRegisterEmp.selected = false;
            this.btnRegisterEmp.Size = new System.Drawing.Size(227, 62);
            this.btnRegisterEmp.TabIndex = 4;
            this.btnRegisterEmp.Text = "Cadastrar Funcionário";
            this.btnRegisterEmp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRegisterEmp.Textcolor = System.Drawing.Color.Black;
            this.btnRegisterEmp.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterEmp.Click += new System.EventHandler(this.btnRegisterEmp_Click);
            // 
            // btnRH
            // 
            this.btnRH.Activecolor = System.Drawing.Color.Transparent;
            this.btnRH.BackColor = System.Drawing.Color.Transparent;
            this.btnRH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRH.BorderRadius = 0;
            this.btnRH.ButtonText = "Recursos Humanos";
            this.btnRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRH.DisabledColor = System.Drawing.Color.Gray;
            this.btnRH.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRH.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRH.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRH.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRH.Iconimage")));
            this.btnRH.Iconimage_right = ((System.Drawing.Image)(resources.GetObject("btnRH.Iconimage_right")));
            this.btnRH.Iconimage_right_Selected = null;
            this.btnRH.Iconimage_Selected = null;
            this.btnRH.IconMarginLeft = 0;
            this.btnRH.IconMarginRight = 0;
            this.btnRH.IconRightVisible = true;
            this.btnRH.IconRightZoom = 0D;
            this.btnRH.IconVisible = true;
            this.btnRH.IconZoom = 40D;
            this.btnRH.IsTab = false;
            this.btnRH.Location = new System.Drawing.Point(0, 217);
            this.btnRH.Margin = new System.Windows.Forms.Padding(5);
            this.btnRH.Name = "btnRH";
            this.btnRH.Normalcolor = System.Drawing.Color.Transparent;
            this.btnRH.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnRH.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnRH.selected = false;
            this.btnRH.Size = new System.Drawing.Size(227, 62);
            this.btnRH.TabIndex = 2;
            this.btnRH.Text = "Recursos Humanos";
            this.btnRH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRH.Textcolor = System.Drawing.Color.Black;
            this.btnRH.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRH.Click += new System.EventHandler(this.btnRH_Click);
            // 
            // btnHome
            // 
            this.btnHome.Activecolor = System.Drawing.Color.Transparent;
            this.btnHome.BackColor = System.Drawing.Color.Transparent;
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.BorderRadius = 0;
            this.btnHome.ButtonText = "Página Inicial";
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.DisabledColor = System.Drawing.Color.Gray;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHome.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Iconcolor = System.Drawing.Color.Transparent;
            this.btnHome.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnHome.Iconimage")));
            this.btnHome.Iconimage_right = null;
            this.btnHome.Iconimage_right_Selected = null;
            this.btnHome.Iconimage_Selected = null;
            this.btnHome.IconMarginLeft = 0;
            this.btnHome.IconMarginRight = 0;
            this.btnHome.IconRightVisible = true;
            this.btnHome.IconRightZoom = 0D;
            this.btnHome.IconVisible = true;
            this.btnHome.IconZoom = 40D;
            this.btnHome.IsTab = false;
            this.btnHome.Location = new System.Drawing.Point(0, 155);
            this.btnHome.Margin = new System.Windows.Forms.Padding(5);
            this.btnHome.Name = "btnHome";
            this.btnHome.Normalcolor = System.Drawing.Color.Transparent;
            this.btnHome.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnHome.OnHoverTextColor = System.Drawing.Color.Black;
            this.btnHome.selected = false;
            this.btnHome.Size = new System.Drawing.Size(227, 62);
            this.btnHome.TabIndex = 1;
            this.btnHome.Text = "Página Inicial";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnHome.Textcolor = System.Drawing.Color.Black;
            this.btnHome.TextFont = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(227, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pnlBar
            // 
            this.pnlBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.pnlBar.Controls.Add(this.btnClose);
            this.pnlBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBar.Location = new System.Drawing.Point(0, 0);
            this.pnlBar.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBar.Name = "pnlBar";
            this.pnlBar.Size = new System.Drawing.Size(1028, 28);
            this.pnlBar.TabIndex = 1;
            this.pnlBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlBar_MouseMove);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(994, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(34, 29);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "x";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 599);
            this.Controls.Add(this.pnlContainer);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1022, 599);
            this.Name = "frmHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHome";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer2.ResumeLayout(false);
            this.pnlMenu.ResumeLayout(false);
            this.pnlFinanceiro.ResumeLayout(false);
            this.pnlRH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.Panel pnlContainer2;
        private System.Windows.Forms.Panel pnlBar;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuFlatButton btnHome;
        private System.Windows.Forms.Panel pnlRH;
        private Bunifu.Framework.UI.BunifuFlatButton btnListEmp;
        private Bunifu.Framework.UI.BunifuFlatButton btnRegisterEmp;
        private Bunifu.Framework.UI.BunifuFlatButton btnRH;
        private System.Windows.Forms.Panel pnlFinanceiro;
        private Bunifu.Framework.UI.BunifuFlatButton btnPagamentosRecebidos;
        private Bunifu.Framework.UI.BunifuFlatButton btnPainelDeControle;
        private Bunifu.Framework.UI.BunifuFlatButton btnFinanceiro;
        private Bunifu.Framework.UI.BunifuFlatButton btnPagamentosEfetuados;
        private System.Windows.Forms.Button btnClose;
    }
}