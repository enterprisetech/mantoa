﻿namespace Mantoã_Arquitetura.Screens.Financeiro.Adicionar_Pagamento.Criar_Pagamento
{
    partial class frmCriarPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBar = new System.Windows.Forms.Panel();
            this.tabControlEX1 = new Dotnetrix.Controls.TabControlEX();
            this.tabPageEX2 = new Dotnetrix.Controls.TabPageEX();
            this.groupBoxEX4 = new Dotnetrix.Controls.GroupBoxEX();
            this.bunifuDatepicker2 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuCustomLabel6 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDatepicker1 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txtnomefuncionario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tabPageEX1 = new Dotnetrix.Controls.TabPageEX();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bunifuCustomLabel11 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel10 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDatepicker3 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuCustomLabel9 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel8 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.bunifuCustomLabel7 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.tabControlEX1.SuspendLayout();
            this.tabPageEX2.SuspendLayout();
            this.groupBoxEX4.SuspendLayout();
            this.tabPageEX1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBar
            // 
            this.pnlBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.pnlBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBar.Location = new System.Drawing.Point(0, 0);
            this.pnlBar.Margin = new System.Windows.Forms.Padding(5);
            this.pnlBar.Name = "pnlBar";
            this.pnlBar.Size = new System.Drawing.Size(534, 26);
            this.pnlBar.TabIndex = 48;
            // 
            // tabControlEX1
            // 
            this.tabControlEX1.Appearance = Dotnetrix.Controls.TabAppearanceEX.FlatButton;
            this.tabControlEX1.BackColor = System.Drawing.Color.White;
            this.tabControlEX1.Controls.Add(this.tabPageEX1);
            this.tabControlEX1.Controls.Add(this.tabPageEX2);
            this.tabControlEX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlEX1.Location = new System.Drawing.Point(0, 26);
            this.tabControlEX1.Name = "tabControlEX1";
            this.tabControlEX1.SelectedIndex = 0;
            this.tabControlEX1.Size = new System.Drawing.Size(534, 327);
            this.tabControlEX1.TabIndex = 49;
            this.tabControlEX1.UseVisualStyles = false;
            // 
            // tabPageEX2
            // 
            this.tabPageEX2.Controls.Add(this.groupBoxEX4);
            this.tabPageEX2.Location = new System.Drawing.Point(4, 25);
            this.tabPageEX2.Name = "tabPageEX2";
            this.tabPageEX2.Size = new System.Drawing.Size(526, 298);
            this.tabPageEX2.TabIndex = 1;
            this.tabPageEX2.Text = "Lançamento";
            // 
            // groupBoxEX4
            // 
            this.groupBoxEX4.Controls.Add(this.bunifuDatepicker2);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel6);
            this.groupBoxEX4.Controls.Add(this.comboBox3);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel5);
            this.groupBoxEX4.Controls.Add(this.bunifuDatepicker1);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel4);
            this.groupBoxEX4.Controls.Add(this.comboBox2);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel3);
            this.groupBoxEX4.Controls.Add(this.comboBox1);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel1);
            this.groupBoxEX4.Controls.Add(this.txtnomefuncionario);
            this.groupBoxEX4.Controls.Add(this.bunifuCustomLabel2);
            this.groupBoxEX4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEX4.Location = new System.Drawing.Point(0, 0);
            this.groupBoxEX4.Name = "groupBoxEX4";
            this.groupBoxEX4.Size = new System.Drawing.Size(526, 295);
            this.groupBoxEX4.TabIndex = 1;
            this.groupBoxEX4.TabStop = false;
            // 
            // bunifuDatepicker2
            // 
            this.bunifuDatepicker2.BackColor = System.Drawing.Color.SeaGreen;
            this.bunifuDatepicker2.BorderRadius = 0;
            this.bunifuDatepicker2.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker2.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.bunifuDatepicker2.FormatCustom = null;
            this.bunifuDatepicker2.Location = new System.Drawing.Point(111, 144);
            this.bunifuDatepicker2.Name = "bunifuDatepicker2";
            this.bunifuDatepicker2.Size = new System.Drawing.Size(58, 28);
            this.bunifuDatepicker2.TabIndex = 18;
            this.bunifuDatepicker2.Value = new System.DateTime(2019, 10, 31, 2, 41, 32, 863);
            // 
            // bunifuCustomLabel6
            // 
            this.bunifuCustomLabel6.AutoSize = true;
            this.bunifuCustomLabel6.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel6.Location = new System.Drawing.Point(6, 225);
            this.bunifuCustomLabel6.Name = "bunifuCustomLabel6";
            this.bunifuCustomLabel6.Size = new System.Drawing.Size(177, 19);
            this.bunifuCustomLabel6.TabIndex = 17;
            this.bunifuCustomLabel6.Text = "Data de compensação:";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(167, 185);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 25);
            this.comboBox3.TabIndex = 16;
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(6, 188);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(155, 19);
            this.bunifuCustomLabel5.TabIndex = 15;
            this.bunifuCustomLabel5.Text = "Pagamento quitado:";
            // 
            // bunifuDatepicker1
            // 
            this.bunifuDatepicker1.BackColor = System.Drawing.Color.SeaGreen;
            this.bunifuDatepicker1.BorderRadius = 0;
            this.bunifuDatepicker1.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.bunifuDatepicker1.FormatCustom = null;
            this.bunifuDatepicker1.Location = new System.Drawing.Point(186, 220);
            this.bunifuDatepicker1.Name = "bunifuDatepicker1";
            this.bunifuDatepicker1.Size = new System.Drawing.Size(58, 28);
            this.bunifuDatepicker1.TabIndex = 14;
            this.bunifuDatepicker1.Value = new System.DateTime(2019, 10, 31, 2, 41, 32, 863);
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(8, 149);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(97, 19);
            this.bunifuCustomLabel4.TabIndex = 13;
            this.bunifuCustomLabel4.Text = "Vencimento:";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(105, 110);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 25);
            this.comboBox2.TabIndex = 12;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(8, 113);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(91, 19);
            this.bunifuCustomLabel3.TabIndex = 11;
            this.bunifuCustomLabel3.Text = "Ocorrência:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(133, 71);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 25);
            this.comboBox1.TabIndex = 10;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(8, 74);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(126, 19);
            this.bunifuCustomLabel1.TabIndex = 9;
            this.bunifuCustomLabel1.Text = "Plano de contas:";
            // 
            // txtnomefuncionario
            // 
            this.txtnomefuncionario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtnomefuncionario.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomefuncionario.ForeColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.HintForeColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.HintText = "";
            this.txtnomefuncionario.isPassword = false;
            this.txtnomefuncionario.LineFocusedColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtnomefuncionario.LineMouseHoverColor = System.Drawing.Color.Black;
            this.txtnomefuncionario.LineThickness = 2;
            this.txtnomefuncionario.Location = new System.Drawing.Point(203, 24);
            this.txtnomefuncionario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnomefuncionario.Name = "txtnomefuncionario";
            this.txtnomefuncionario.Size = new System.Drawing.Size(294, 33);
            this.txtnomefuncionario.TabIndex = 8;
            this.txtnomefuncionario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(8, 31);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(192, 19);
            this.bunifuCustomLabel2.TabIndex = 5;
            this.bunifuCustomLabel2.Text = "Descrição do pagamento:";
            // 
            // tabPageEX1
            // 
            this.tabPageEX1.Controls.Add(this.textBox1);
            this.tabPageEX1.Controls.Add(this.bunifuCustomLabel11);
            this.tabPageEX1.Controls.Add(this.bunifuCustomLabel10);
            this.tabPageEX1.Controls.Add(this.bunifuDatepicker3);
            this.tabPageEX1.Controls.Add(this.bunifuCustomLabel9);
            this.tabPageEX1.Controls.Add(this.comboBox6);
            this.tabPageEX1.Controls.Add(this.bunifuCustomLabel8);
            this.tabPageEX1.Controls.Add(this.comboBox5);
            this.tabPageEX1.Controls.Add(this.bunifuCustomLabel7);
            this.tabPageEX1.Controls.Add(this.comboBox4);
            this.tabPageEX1.Location = new System.Drawing.Point(4, 25);
            this.tabPageEX1.Name = "tabPageEX1";
            this.tabPageEX1.Size = new System.Drawing.Size(526, 298);
            this.tabPageEX1.TabIndex = 2;
            this.tabPageEX1.Text = "Outras informações";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(130, 210);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(388, 81);
            this.textBox1.TabIndex = 20;
            // 
            // bunifuCustomLabel11
            // 
            this.bunifuCustomLabel11.AutoSize = true;
            this.bunifuCustomLabel11.Location = new System.Drawing.Point(24, 210);
            this.bunifuCustomLabel11.Name = "bunifuCustomLabel11";
            this.bunifuCustomLabel11.Size = new System.Drawing.Size(96, 17);
            this.bunifuCustomLabel11.TabIndex = 19;
            this.bunifuCustomLabel11.Text = "Observações:";
            // 
            // bunifuCustomLabel10
            // 
            this.bunifuCustomLabel10.AutoSize = true;
            this.bunifuCustomLabel10.Location = new System.Drawing.Point(7, 155);
            this.bunifuCustomLabel10.Name = "bunifuCustomLabel10";
            this.bunifuCustomLabel10.Size = new System.Drawing.Size(158, 17);
            this.bunifuCustomLabel10.TabIndex = 18;
            this.bunifuCustomLabel10.Text = "Data de competência:";
            // 
            // bunifuDatepicker3
            // 
            this.bunifuDatepicker3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(197)))), ((int)(((byte)(60)))));
            this.bunifuDatepicker3.BorderRadius = 0;
            this.bunifuDatepicker3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuDatepicker3.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.bunifuDatepicker3.FormatCustom = null;
            this.bunifuDatepicker3.Location = new System.Drawing.Point(168, 150);
            this.bunifuDatepicker3.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuDatepicker3.Name = "bunifuDatepicker3";
            this.bunifuDatepicker3.Size = new System.Drawing.Size(185, 26);
            this.bunifuDatepicker3.TabIndex = 17;
            this.bunifuDatepicker3.Value = new System.DateTime(2019, 10, 15, 0, 0, 0, 0);
            // 
            // bunifuCustomLabel9
            // 
            this.bunifuCustomLabel9.AutoSize = true;
            this.bunifuCustomLabel9.Location = new System.Drawing.Point(3, 111);
            this.bunifuCustomLabel9.Name = "bunifuCustomLabel9";
            this.bunifuCustomLabel9.Size = new System.Drawing.Size(117, 17);
            this.bunifuCustomLabel9.TabIndex = 16;
            this.bunifuCustomLabel9.Text = "Centro de custo:";
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(130, 108);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(94, 25);
            this.comboBox6.TabIndex = 15;
            // 
            // bunifuCustomLabel8
            // 
            this.bunifuCustomLabel8.AutoSize = true;
            this.bunifuCustomLabel8.Location = new System.Drawing.Point(3, 61);
            this.bunifuCustomLabel8.Name = "bunifuCustomLabel8";
            this.bunifuCustomLabel8.Size = new System.Drawing.Size(85, 17);
            this.bunifuCustomLabel8.TabIndex = 14;
            this.bunifuCustomLabel8.Text = "Fornecedor:";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(94, 58);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(94, 25);
            this.comboBox5.TabIndex = 13;
            // 
            // bunifuCustomLabel7
            // 
            this.bunifuCustomLabel7.AutoSize = true;
            this.bunifuCustomLabel7.Location = new System.Drawing.Point(7, 15);
            this.bunifuCustomLabel7.Name = "bunifuCustomLabel7";
            this.bunifuCustomLabel7.Size = new System.Drawing.Size(70, 17);
            this.bunifuCustomLabel7.TabIndex = 12;
            this.bunifuCustomLabel7.Text = "Entidade:";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(83, 12);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(94, 25);
            this.comboBox4.TabIndex = 11;
            // 
            // frmCriarPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 354);
            this.Controls.Add(this.tabControlEX1);
            this.Controls.Add(this.pnlBar);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCriarPagamento";
            this.Text = "frmCriarPagamento";
            this.tabControlEX1.ResumeLayout(false);
            this.tabPageEX2.ResumeLayout(false);
            this.groupBoxEX4.ResumeLayout(false);
            this.groupBoxEX4.PerformLayout();
            this.tabPageEX1.ResumeLayout(false);
            this.tabPageEX1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBar;
        private Dotnetrix.Controls.TabControlEX tabControlEX1;
        private Dotnetrix.Controls.TabPageEX tabPageEX2;
        private Dotnetrix.Controls.GroupBoxEX groupBoxEX4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel6;
        private System.Windows.Forms.ComboBox comboBox3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private System.Windows.Forms.ComboBox comboBox2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private System.Windows.Forms.ComboBox comboBox1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtnomefuncionario;
        private Dotnetrix.Controls.TabPageEX tabPageEX1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel11;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel10;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel9;
        private System.Windows.Forms.ComboBox comboBox6;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel8;
        private System.Windows.Forms.ComboBox comboBox5;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel7;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox1;
    }
}