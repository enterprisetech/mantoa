﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantoã_Arquitetura.Screens.Financeiro.Adicionar_Pagamento
{
    public partial class frmAdicionarPagamento : Form
    {
        public frmAdicionarPagamento()
        {
            InitializeComponent();
        }

        private void bunifuCustomDataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
        }

        private void btnAdicionarPagamento_Click(object sender, EventArgs e)
        {
            Criar_Pagamento.frmCriarPagamento criarpagamento = new Criar_Pagamento.frmCriarPagamento();
            criarpagamento.Show();
        }
    }
}
