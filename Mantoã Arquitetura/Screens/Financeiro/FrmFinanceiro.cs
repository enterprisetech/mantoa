﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantoã_Arquitetura.Screens.Financeiro
{
    public partial class FrmFinanceiro : Form
    {
        public FrmFinanceiro()
        {
            InitializeComponent();
        }
        private void AbrirFormulario<MiForm>() where MiForm : Form, new()
        {
            Form formulario;
            formulario = pnlContainer.Controls.OfType<MiForm>().FirstOrDefault();//Busca en la colecion el formulario
                                                                                  //si el formulario/instancia no existe
            if (formulario == null)
            {
                formulario = new MiForm();
                formulario.TopLevel = false;
                formulario.FormBorderStyle = FormBorderStyle.None;
                formulario.Dock = DockStyle.Fill;
                pnlContainer.Controls.Add(formulario);
                pnlContainer.Tag = formulario;
                formulario.Show();
                formulario.BringToFront();
            }
            //si el formulario/instancia existe
            else
            {
                formulario.BringToFront();
            }
        }

        private void btnNovoCliente_Click(object sender, EventArgs e)
        {

            /// Instancias
            /// 
            frmNovoClient novocliente = new frmNovoClient();
            novocliente.Show();

        }

        private void btnDividas_Click(object sender, EventArgs e)
        {
        
        }
    }
}
