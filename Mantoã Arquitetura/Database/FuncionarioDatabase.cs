﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantoã_Arquitetura.Database
{
    class FuncionarioDatabase
    {

        public void InserirFuncionario(Model.Entities.tb_employees modelo)
        {
            Model.Entities.Database db = new Model.Entities.Database();
            db.tb_employees.Add(modelo);
            db.SaveChanges();
        }

        public List<Model.Entities.tb_employees> ConsultarFuncionario()
        {
            Model.Entities.Database db = new Model.Entities.Database();
            List<Model.Entities.tb_employees> Lista =  db.tb_employees.ToList();

            return Lista;

        }
                

    }
}
