﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantoã_Arquitetura.Database
{
    class FuncionarioContaDatabase
    {

        public void InserirConta(Model.Entities.tb_contafuncionario model)
        {
            Model.Entities.Database db = new Model.Entities.Database();
            db.tb_contafuncionario.Add(model);
            db.SaveChanges();
        }

        public Model.Entities.tb_contafuncionario ConsultarConta(string user, string pass)
        {
            Model.Entities.Database db = new Model.Entities.Database();
            Model.Entities.tb_contafuncionario contas = db.tb_contafuncionario.FirstOrDefault(t => t.nm_usuario == user && t.pw_senha == pass);
            return contas;
        }




    }
}
