﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mantoã_Arquitetura.Model.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Database : DbContext
    {
        public Database()
            : base("name=Database")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tb_bancofuncionario> tb_bancofuncionario { get; set; }
        public virtual DbSet<tb_cargo> tb_cargo { get; set; }
        public virtual DbSet<tb_cliente> tb_cliente { get; set; }
        public virtual DbSet<tb_contafuncionario> tb_contafuncionario { get; set; }
        public virtual DbSet<tb_departamento> tb_departamento { get; set; }
        public virtual DbSet<tb_despesas> tb_despesas { get; set; }
        public virtual DbSet<tb_documentofuncionario> tb_documentofuncionario { get; set; }
        public virtual DbSet<tb_employees> tb_employees { get; set; }
        public virtual DbSet<tb_enderecocliente> tb_enderecocliente { get; set; }
        public virtual DbSet<tb_enderecofornecedor> tb_enderecofornecedor { get; set; }
        public virtual DbSet<tb_enderecofuncionario> tb_enderecofuncionario { get; set; }
        public virtual DbSet<tb_fornecedor> tb_fornecedor { get; set; }
        public virtual DbSet<tb_produto> tb_produto { get; set; }
        public virtual DbSet<tb_salariofuncionario> tb_salariofuncionario { get; set; }
    }
}
