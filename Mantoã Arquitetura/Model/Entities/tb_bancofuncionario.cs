//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mantoã_Arquitetura.Model.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_bancofuncionario
    {
        public int id_bancofuncionario { get; set; }
        public int id_emp { get; set; }
        public string nm_banco { get; set; }
        public string cd_agencia { get; set; }
        public string cd_contaCorrente { get; set; }
    }
}
